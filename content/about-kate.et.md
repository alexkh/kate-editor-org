---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: Omadused
---
## Rakenduse omadused

![Screenshot showing Kate window splitting feature and search & replace plugin](/images/kate-window.webp)

+ Võimalus näha ja muuta mitut dokumenti korraga, poolitades akent nii rõht- kui püstsuunas
+ Lot of plugins: [Embedded terminal](https://konsole.kde.org), SQL plugin, build plugin, GDB plugin, Replace in Files, and more
+ Mitmedokumendiliides (MDI)
+ Seansside toetus

## Üldised omadused

![Kate ekraanipilt otsimise ja asendamise võimalusega](/images/kate-search-replace.png)

+ Kodeeringute toetus (Unicode ja veel palju teisi)
+ Kahesuunalise teksti renderdamise toetus
+ Realõppude toetus (Windows, Unix, Mac), kaasa arvatud automaatne tuvastamine
+ Võrgu läbipaistvus (võrgufailide avamine)
+ Laiendamine skriptide abil

## Muud redaktori omadused

![Kate piirde ekraanipilt reanumbri ja järjehoidjaga](/images/kate-border.png)

+ Järjehoidjate süsteem (toetatud on ka katkestuspunktid jmes)
+ Kerimisriba märgised
+ Rea muutmise tähised
+ Reanumbrid
+ Koodi voltimine

## Süntaksi esiletõstmine

![Kate ekraanipilt süntaksi esiletõstmisega](/images/kate-syntax.png)

+ Esiletõstmise toetus rohkem kui 300 keeles
+ Sulgude sobitamine
+ Nutikas reaalajas õigekirja kontroll
+ Valitud sõnade esiletõstmine

## Programmeerimisomadused

![Kate ekraanipilt programmeerimisvõimalustega](/images/kate-programming.png)

+ Skriptitav automaatne treppimine
+ Nutikas kommenteerimise ja kommenteerimise eemaldamise käitlemine
+ Automaatne lõpetamine argumentide vihjetega
+ Vi input mode
+ Ristkülikukujulise plokkvalimise režiim

## Otsimine ja asendamine

![Kate ekraanipilt inkrementotsingu võimalusega](/images/kate-search.png)

+ Inkrementotsing, tuntud ka kui &#8220;otsimine kirjutamise ajal&#8221;
+ Mitmel real otsimise ja asendamise toetus
+ Regulaaravaldiste toetus
+ Otsimine ja asendamine mitmes avatud failis või kettal paiknevates failides

## Varundamine ja taastamine

![Kate ekraanipilt krahhijärgse taastamise võimalusega](/images/kate-crash.png)

+ Varukoopiad salvestamisel
+ Swap files to recover data on system crash
+ Tagasivõtmiste ja uuestitegemiste süsteem
