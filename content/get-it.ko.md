---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Kate 다운로드
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### 리눅스와 유닉스

+ [배포판](https://kde.org/distributions/)에서 [Kate](https://apps.kde.org/ko/kate)나 [KWrite](https://apps.kde.org/ko/kwrite) 설치

{{< appstream_badges >}}

+ [Snapcraft의 Kate Snap 패키지](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate 릴리스(64비트) AppImage 패키지](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Kate 일일 빌드 (64비트) AppImage 패키지](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ 소스 코드에서 [빌드](/build-it/#linux)할 수 있습니다.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Microsoft 스토어의 Kate](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Chocolatey에서 Kate 다운로드](https://chocolatey.org/packages/kate)
+ [Kate 릴리스(64비트) 설치 도구](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Kate 일일 빌드(64비트) 설치 도구](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ 소스 코드에서 [빌드](/build-it/#windows)할 수 있습니다.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Kate 릴리스 설치 도구](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Kate 일일 빌드 설치 도구](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ 소스 코드에서 [빌드](/build-it/#mac)할 수 있습니다.

{{< /get-it >}}

{{< get-it-info >}}

**릴리스 정보:** <br> [Kate](https://apps.kde.org/ko/kate)와 [KWrite](https://apps.kde.org/ko/kwrite)는 [년간 3회 릴리스되는](https://community.kde.org/Schedules) [KDE 프로그램](https://apps.kde.org/ko/)의 일부로 출시됩니다. [텍스트 편집](https://api.kde.org/frameworks/ktexteditor/html/)과 [구문 강조](https://api.kde.org/frameworks/syntax-highlighting/html/) 엔진은 [매월마다 업데이트되는](https://community.kde.org/Schedules/Frameworks) [KDE 프레임워크](https://kde.org/announcements/kde-frameworks-5.0/)에서 제공합니다. 새 릴리스는 [여기](https://kde.org/announcements/)에 공지됩니다.

\* **릴리스** 패키지는 Kate와 KDE 프레임워크 최신 버전을 포함합니다.

\*\* **일일 빌드** 패키지는 매일 소스 코드에서 자동으로 컴파일되며, 불안정하거나 버그 또는 불완전한 기능이 있을 수도 있습니다. 테스트 목적으로만 추천합니다.

{{< /get-it-info >}}
