---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: De mascotte van Kate
---
De mascotte van Kate, Kate the Cyber Woodpecker, is ontworpen door [Tyson Tan](https://www.tysontan.com/).

De huidige evolutie van onze mascotte is voor het eerst onthuld [in april 2021](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## Kate the Cyber Woodpecker

![Kate the Cyber Woodpecker](/images/mascot/electrichearts_20210103_kate_normal.png)

## Versie zonder voettekst

![Kate the Cyber Woodpecker - zonder tekst](/images/mascot/electrichearts_20210103_kate_notext.png)

## Versie zonder achtergrond

![Kate the Cyber Woodpecker - zonder achtergrond](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Licentie

Voor de licentie, om Tyson aan te halen:

> Hierbij doneer ik de nieuwe Kate the Cyber Woodpecker aan het Kate Editor project. De illustraties hebben een tweetal licenties onder LGPL (of wat dezelfde is als Kate Editor) en Creative Commons BY-SA. Op deze manier is het niet nodig mij te vragen om toestemming voor gebruik/wijziging van de mascotte.

## Ontwerpgeschiedenis van Kate the Cyber Woodpecker

Ik heb Tyson gevraagd of ik de tussenliggende versies die we hebben uitgewisseld om de evolutie te tonen en hij gaf toestemming en schreef zelfs enkele korte samenvattingen over de individuele stappen. Dit omvat de initiele mascotte en het pictogram dat paste in de evolutie van het ontwerp. Daarom haal ik onderstand alleen maar aan wat hij schreef over de verschillende stappen.

### 2014 - Kate the Woodpecker

![2014 version - Kate the Woodpecker](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> De 2014 versie is waarschijnlijk gedaan op mijn laagste punt als artiest. I had jaren niet veel meer getekend en stond op de rand om het helemaal op te geven. Er waren enige interessante ideeën in deze versie, namelijk de {} accolades op haar borst en het kleurenschema geïnspireerd door de accentuering van code met doxygen. De uitvoering was echter erg simplistisch -- op dat moment stopte ik bewust met het gebruik van elke geavanceerde technieken om mijn onvoldoende basisvaardigheden te tonen. In de volgende jaren, zou ik door een zwaar proces gaan om mijn gebroken, zelfstudie fundatie als kunstenaar van de grond af aan opnieuw op te bouwen. Ik ben dankbaar dat het team van Kate deze vele jaren op hun website hebben gehouden.

### 2020 - het nieuwe Kate pictogram

![2020 - Het nieuwe pictogram van Kate](/images/mascot/history/kate-3000px.png)

> Het nieuw pictogram van Kate was de enige keer dat ik echte vector illustraties heb geprobeerd. Het was op dat moment dat ik begon om enig zwak artistiek gevoel te ontwikkelen. Ik had geen eerdere ervaring met ontwerpen van pictogrammen, er was nul discipline zoals rekenkundig geleide verhoudingen. Elke vorm en kromme was gemaakt met mijn instinct als artiest. Het concept was erg simpel: het is een vogelvorm lijkend op de letter "K" en het moet scherp zijn. De vogel was brokkeliger totdat Christoph dat opmerkte, later heb ik het omgevormd in een more gestroomlijnde vorm.

### 2021 - Mascotte schets

![2021 version - Schets van de nieuwe mascotte](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> In deze initiële schets I was verleid om het robotachtige concept te laten vallen en in plaats daarvan gewoon een leuke goed uitziende vogel te tekenen.

### 2021 - Mascotte ontwerp

![2021 version - Herontwerp van de nieuwe mascotte](/images/mascot/history/electrichearts_20210103_kate_design.png)

> Het herontwerp van de mascotte van Kate werd opnieuw opgepakt nadat ik de nieuwe startafbeelding voor het aankomende Krita 5.0 had beëindigd. Ik was op dat moment in staat mijzelf steeds meer te motiveren elke keer dat ik een nieuwe afbeelding probeerde, ik had besloten om het robotachtige concept te behouden. Hoewel de proporties niet goed waren, waren de mechanische ontwerpelementen min of meer in deze opgesloten.

### 2021 - Uiteindelijke Mascotte

![2021 version - Uiteindelijke Kate the Cyber Woodpecker](/images/mascot/electrichearts_20210103_kate_normal.png)

> De proporties zijn, nadat Christoph die had aangewezen, aangepast. De pose is aangepast om comfortabeler aan te voelen. De vleugels zijn opnieuw getekend na bestudering van vleugels van echt vogels. Een mooi uitziend, gedetailleerd toetsenbord was met de hand getekend om het hoofdgebruik van de toepassing te benadrukken. Een abstracte achtergrond was toegevoegd met een toepasselijk 2D typografisch ontwerp. Het algemene idee is: onmiskenbaar geavanceerd en kunstmatig, maar ook onmiskenbaar vol leven en menselijk.
