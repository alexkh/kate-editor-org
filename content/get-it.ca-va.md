---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Obteniu Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux i Unixs

+ Instal·leu el [Kate](https://apps.kde.org/ca/kate) o el [KWrite](https://apps.kde.org/ca/kwrite) des de la [vostra distribució](https://www.kde.org/ca/distributions/)

{{< appstream_badges >}}

+ El [paquet Snap de Kate a Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Paquet AppImage de llançament de Kate (64bits)](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Paquet AppImage nocturn de Kate (64bits)](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Construïu-lo](/build-it/#linux) a partir del codi font.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ El [Kate a la Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ El [Kate via Chocolatey](https://chocolatey.org/packages/kate)
+ L'[instal·lador del llançament de Kate (64bits)](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ L'[instal·lador de la versió nocturna de Kate (64bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Construïu-lo](/build-it/#windows) a partir del codi font.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ L'[instal·lador del llançament de Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ L'[instal·lador de la versió nocturna de Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Construïu-lo](/build-it/#mac) a partir del codi font.

{{< /get-it >}}

{{< get-it-info >}}

**Quant als llançaments:** <br>El [Kate](https://apps.kde.org/ca/kate) i el [KWrite](https://apps.kde.org/ca/kwrite) són part de les [aplicacions KDE](https://apps.kde.org/ca), que es [publiquen normalment 3 vegades a l'any de forma completa](https://community.kde.org/Schedules). El [motor d'edició de text](https://api.kde.org/frameworks/ktexteditor/html/) i el [ressaltat de sintaxi](https://api.kde.org/frameworks/syntax-highlighting/html/) estan proporcionats pels [Frameworks de KDE](https://kde.org/ca/announcements/kde-frameworks-5.0/), que s'[actualitzen mensualment](https://community.kde.org/Schedules/Frameworks). Els llançaments nous s'anuncien [ací](https://kde.org/ca/announcements/).

\* Els paquets del **llançament** contenen l'última versió de Kate i de Frameworks de KDE.

\*\* Els paquets **nocturns** es compilen automàticament cada dia a partir del codi font, i per tant, poden ser inestables i poden haver-hi errors o funcionalitats incompletes. Només són recomanats amb la finalitat de fer proves.

{{< /get-it-info >}}
