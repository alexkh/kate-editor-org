---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: Mascota de Kate
---
La mascota de Kate, Kate el Ciberpicot («Cyber Woodpecker»), fou dissenyat per [Tyson Tan](https://www.tysontan.com/).

L'evolució actual de la nostra mascota es va donar a conéixer per primera vegada [l'abril de 2021](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## Kate el Ciberpicot

![Kate el Ciberpicot](/images/mascot/electrichearts_20210103_kate_normal.png)

## Versió sense text al peu

![Kate el Ciberpicot - Sense text](/images/mascot/electrichearts_20210103_kate_notext.png)

## Versió sense fons

![Kate el Ciberpicot - Sense fons](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Llicències

Per a les llicències, i citant a Tyson:

> Jo done «Kate the Cyber Woodpecker» al projecte de l'editor Kate. Els treballs artístics tenen la llicència dual segons la LGPL (o la mateixa que l'editor Kate) i la Creative Commons BY-SA. D'esta manera no m'heu de demanar permís abans d'utilitzar/modificar la mascota.

## Història del disseny de Kate el Ciberpicot («Cyber Woodpecker»)

Vaig demanar a Tyson si podia compartir les versions intermèdies que vàrem intercanviar per a mostrar l'evolució i ell hi estigué d'acord, inclús va escriure resums curts dels passos individuals. Açò inclou la mascota inicial i les icones que s'ajusten en esta evolució del disseny. Per tant, davall citarem el que va escriure quant als diferents passos.

### 2014 - Kate el Ciberpicot

![Versió de 2014 - Kate el Ciberpicot](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> La versió de 2014 potser es va fer durant el meu moment més baix com a artista. No havia dibuixat gaire en els darrers anys, i estava a punt d'abandonar-ho completament. Hi havia algunes idees interessants en esta versió, com les claus {} del seu pit, i l'esquema de color inspirat en el ressaltat de codi de Doxygen. Tanmateix, l'execució era molt senzilla -- en aquell moment vaig deixar d'emprar deliberadament qualsevol tècnica sofisticada que fes evident els meus coneixements bàsics inadequats. Els anys següents vaig seguir un laboriós procés per a reconstruir la base artística autodidacta des del principi. Agraeixo a l'equip de Kate que encara mantingen esta versió en el seu lloc web durant molts anys.

### 2020 - La icona nova de Kate

![2020 - La icona nova de Kate](/images/mascot/history/kate-3000px.png)

> La icona nova de Kate ha sigut l'única vegada que he intentat un gràfic vectorial real. Fou en aquell moment que vaig començar a desenvolupar un feble sentit artístic. No tenia cap experiència prèvia en el disseny d'icones, no hi havia cap formació en la proporció basada en matemàtiques. Cada forma i cada corba es van retocar utilitzant el meu instint com a artista. El concepte era molt senzill: és un ocell en forma de la lletra «K», i cal que siga angulós. L'ocell era més cepat fins que en Christoph ho va advertir, i després el vaig retocar per fer un forma més pulcra.

### 2021 - Esbós de la mascota

![Versió de 2021 - Esbós de la mascota nova](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> En este esbós inicial, vaig estar temptat de desfer-me del concepte robòtic i dibuixar en el seu lloc només un ocell bonic i agradable.

### 2021 - Disseny de la mascota

![Versió de 2021- Redisseny de la mascota nova](/images/mascot/history/electrichearts_20210103_kate_design.png)

> Vaig reprendre el redisseny de la mascota de Kate després de finalitzar el dibuix de la imatge nova de la pantalla de presentació per al proper Krita 5.0. Podia forçar-me a fer més cada vegada que intentava una imatge nova a la vegada, de manera que estava decidit a mantindre el concepte robòtic. Encara que no hi havia proporcions, els elements mecànics de disseny ja estaven més o menys ideats.

### 2021 - Mascota final

![Versió de 2021 - Kate el Ciberpicot final](/images/mascot/electrichearts_20210103_kate_normal.png)

> La proporció es va ajustar després que Christoph ho assenyalara. La postura es va retocar per a fer-la més confortable. Es van redibuixar les ales després d'estudiar les ales dels ocells reals. Es va dibuixar a mà un teclat atractiu detallat per a emfatitzar l'ús principal de l'aplicació. Es va afegir un fons abstracte amb un disseny tipogràfic 2D apropiat. La idea general és: inequívocament innovador i artificial, però també inequívocament ple de vida i humanitat.
