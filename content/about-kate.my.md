---
author: ခရစ်စ်တိုကူမန်း
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: စွမ်းဆောင်ရည်များ
---
## အပ္ပလီကေးရှင်းစွမ်းဆောင်ရည်များ

![Screenshot showing Kate window splitting feature and search & replace plugin](/images/kate-window.webp)

+ အထက်၊အောက် သို့မဟုတ် ဘယ်၊ညာ ဝင်းဒိုးခွဲပြီး မှတ်တမ်းပုံနှိပ်မူများကို တစ်ချိန်တည်း တည်းဖြတ်၍ ကြည့်ပါ
+ ပလပ်ဂင်အများကြီး - [Embedded terminal](https://konsole.kde.org)၊ အက်စ်ကျူအယ်ပလပ်ဂင်၊ တည်ဆောက်မှုပလပ်ဂင်၊ ဂျီဒီဘီပလပ်ဂင်၊ ဖိုင်လ်များတွင် အစားထိုးမှု နှင့် နောက်ထပ်ပလပ်ဂင်များ
+ မှတ်တမ်းပုံနှိပ်မူအစုံ အင်တာဖေ့စ် (အမ်ဒီအိုင်)
+ ဆက်ရှင် ထောက်ပံ့မှု

## အထွေထွေစွမ်းဆောင်ရည်များ

![Screenshot showing Kate search and replace feature](/images/kate-search-replace.png)

+ ကုတ်ဝှက်စနစ်ထောက်ပံ့မှု (ကူနီကုတ်နှင့် အခြားစနစ်များ)
+ လားရာနှစ်ခု စာသား ရေးဆွဲဖော်ပြခြင်းထောက်ပံ့မှု
+ အလိုအလျောက်သိရှိမှုအပါအဝင် စာကြောင်းအဆုံးသတ်သင်္ကေတထောက်ပံ့မှု (ဝင်းဒိုးစ်၊ ယူနစ်စ်၊ မက်စ်)
+ ဆ.သ.ရ-ကွန်ရက် ထင်သာမြင်သာရှိမှု (အဝေးဖိုင်လ်များဖွင့်)
+ ညွှန်ကြားကုတ်ရေး၍ တိုးချဲ့နိုင်သည်

## အဆင့်မြင့်စာသားတည်းဖြတ်ကိရိယာ စွမ်းဆောင်ရည်များ

![Screenshot of Kate border with line number and bookmark](/images/kate-border.png)

+ စာမျက်နှာအမှတ်အသားစနစ် ( စာခွဲအမှတ်များ စသည်တို့ကိုလည်းထောက်ပံ့ပါသည်)
+ လှိမ့်ဆွဲတန်း အမှတ်များ
+ စာကြောင်းမွမ်းမံမှုညွှန်ပြချက်များ
+ စာကြောင်းနံပါတ်များ
+ ကုတ် ခေါက်ခြင်း

## အထားအသို အရောင်တင်ပြနိုင်မှု

![Screenshot of Kate syntax highlighting features](/images/kate-syntax.png)

+ ဘာသာစကား ၃၀၀ ကျော်အတွက် အလင်းတင်ပြခြင်းထောက်ပံ့မှု
+ ကွင်းစကွင်းပိတ်
+ အလိုက်တသိ စာရေးစဥ်တန်း၍စာလုံးပေါင်းစစ်ဆေးခြင်း
+ ရွေးချယ်ထားသောစာလုံးများကို အရောင်တင်ပြမည်

## ပရိုဂရမ်မင်းဆိုင်ရာစွမ်းဆောင်ရည်များ

![Screenshot of Kate programming features](/images/kate-programming.png)

+ ညွှန်ကြားကုတ်ရေး၍ရသော အလိုအလျောက်အင်တင်းခြင်း
+ အလိုက်တသိ မှတ်ချက်ရေးခြင်း၊ဖျက်ခြင်း ကိုင်တွယ်မှု
+ ပေးချက်သဲလွန်စများနှင့် စာလုံး အလိုအလျောက်ပြီးမြောက်မှု
+ VIစာရေးသွင်းနည်း
+ ထောင့်မှန်စတုဂံအကွက်ရွေးချယ်ထုံးနည်း

## ရှာဖွေ၊အစားထိုး

![Screenshot of Kate incremental search feature](/images/kate-search.png)

+ &#8220; အဖြစ်အသိများသော တစ်ခုချင်းတိုးတိုးရှာဖွေမှုသည် သင်ရေးသည်&#8221; နှင့် ရှာသည်
+ စာကြောင်းအစုံ ရှာဖွေ၊အစားထိုးမှု ပြုနိုင်သည်
+ ပုံမှန်အသုံးအနှုန်းများ ထောက်ပံ့မှု
+ ဖွင့်ထားသော ဖိုင်လ် သို့ ဒစ်စ်ပေါ်က ဖိုင်လ် အများအပြားတွင် ရှာဖွေ၊ အစားထိုးမည်

## ဘက်ခ်အပ် နှင့် ရီစတိုး

![Screenshot of Kate recover from crash feature](/images/kate-crash.png)

+ သိမ်းဆည်းသည်နှင့် ဘက်ခ်အပ်လုပ်မည်
+ စစ်စတမ်ပျက်ရာတွင် ဒေတာပြန်ရရန် ဖိုင်လ်များ လဲမည်
+ ခေျခြင်း / ပြန်လုပ်ခြင်း စနစ်
