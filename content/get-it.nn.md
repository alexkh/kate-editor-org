---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Last ned Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unix-ar

+ Installer [Kate](https://apps.kde.org/en/kate) eller [KWrite](https://apps.kde.org/en/kwrite) frå [distribusjonen din](https://kde.org/distributions/)

{{< appstream_badges >}}

+ [Snap-pakke for Kate i Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate's release (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Kate's nightly (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Kompiler](/build-it/#linux) frå kjeldekoden.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate i Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)
+ [Kate release (64bit) installer](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Installerings­program for dagleg oppdatert Kate-versjon (64 bit)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Kompiler](/build-it/#windows) frå kjeldekoden.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Installerings­program for offisiell Kate-versjon](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Installerings­program for dagleg oppdatert Kate-versjon](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Kompiler](/build-it/#mac) frå kjeldekoden.

{{< /get-it >}}

{{< get-it-info >}}

**Dei ulike utgåvene:** <br>[Kate](https://apps.kde.org/en/kate) og [KWrite](https://apps.kde.org/en/kwrite) er ein del av [KDE Applications](https://apps.kde.org), som [vert utgjeven om lag tre gongar i året](https://community.kde.org/Schedules). [Skrivekomponenten](https://api.kde.org/frameworks/ktexteditor/html/) og [syntaks­merkinga](https://api.kde.org/frameworks/syntax-highlighting/html/) kjem frå [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), som [vert utgjeven månadleg](https://community.kde.org/Schedules/Frameworks). Nye utgåver vert [kunngjorde her](https://kde.org/announcements/).

\* Dei **offisielle** pakkane inneheld nyaste versjon av Kate og KDE Frameworks.

\*\* Dei **dagleg** oppdaterte pakkane vert automatisk kompilerte frå kjeldekoden. Dei kan derfor vera noko ustabile og innehalda feil og nye funksjonar som ikkje verkar heilt enno. Du bør berre bruka dei til testing.

{{< /get-it-info >}}
