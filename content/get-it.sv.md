---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Hämta Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux och Unix

+ Installera [Kate](https://apps.kde.org/en/kate) eller [Kwrite](https://apps.kde.org/en/kwrite) från din [distribution](https://kde.org/distributions/)

{{< appstream_badges >}}

+ [Kates Snap-paket på Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kates 64-bitars utgåva Appimage-paket](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Kates nattliga 64-bitars AppImage-paket](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Bygg det](/build-it/#linux) från källkod.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate på Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)
+ [Installationsverktyg för Kate-utgåva (64-bitar)](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Kates nattliga (64-bitars) installationsverktyg](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Bygg det](/build-it/#windows) från källkod.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Kates installationsverktyg för utgåvor](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Kates nattliga installationsverktyg](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Bygg det](/build-it/#mac) från källkod.

{{< /get-it >}}

{{< get-it-info >}}

**Om utgåvorna:** <br>[Kate](https://apps.kde.org/en/kate) och [Kwrite](https://apps.kde.org/en/kwrite) ingår i [KDE Program](https://apps.kde.org), som normalt [ges ut tillsammans tre gånger om året](https://community.kde.org/Schedules). Gränssnitten för [textredigering](https://api.kde.org/frameworks/ktexteditor/html/) och [syntaxfärgläggning](https://api.kde.org/frameworks/syntax-highlighting/html/) tillhandahålls av [KDE Ramverk](https://kde.org/announcements/kde-frameworks-5.0/), som [uppdateras månadsvis](https://community.kde.org/Schedules/Frameworks). Nya utgåvor tillkännages [här](https://kde.org/announcements/).

\* Paketen benämnda **utgåvor** innehåller senaste versionen av Kate och KDE Ramverk.

\*\* De **nattliga** paketen kompileras automatiskt från källkod, och kan därför vara instabila och innehålla fel eller ofullständiga funktioner. De rekommenderas bara i utprovningssyfte.

{{< /get-it-info >}}
