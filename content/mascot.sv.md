---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: Kates maskot
---
Kates maskot, Cyberhackspetten Kate, designades av [Tyson Tan](https://www.tysontan.com/).

Den aktuella utvecklingen av vår maskot avslöjades [april 2021](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## Cyberhackspetten Kate

![Cyberhackspetten Kate](/images/mascot/electrichearts_20210103_kate_normal.png)

## Version utan text

![Cyberhackspetten Kate - utan text](/images/mascot/electrichearts_20210103_kate_notext.png)

## Version utan bakgrund

![Cyberhackspetten Kate - utan bakgrund](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Licens

När det gäller licensen, för att citera Tyson:

> Härmed donerar jag den nya Cyberhackspetten Kate till editorprojektet Kate. Grafiken är dubbelt licensierad med LGPL (eller vad som än är samma som editorn Kate) och Creative Commons BY-SA. På så sätt behöver ni inte fråga mig om rättighet innan maskoten används eller ändras.

## Designhistorik för cyberhackspetten Kate

Jag frågade Tyson om jag kan dela med mig av de mellanliggande versionerna vi utbytte för att visa utvecklingen. Han gick med på det, och skrev även några korta sammanfattningar om de individuella stegen. Det inkluderar den ursprungliga maskoten och ikonen som passar in i designutvecklingen. Därför citerar jag bara vad han skrev om de olika stegen nedan.

### 2014 - Hackspetten Kate

![Version från 2014 - Hackspetten Kate](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> Versionen från 2014 gjordes under vad som kanske var mitt bottenläge som konstnär. Jag hade inte ritat mycket under åratal, och var på gränsen att ge upp helt och hållet. Det fanns några intressanta idéer i den här versionen, närmare bestämt {} på bröstet och färgschemat inspirerat av färgläggning av doxygen-kod. Dock var utförandet mycket naivt: vid den här tidpunkten hade jag avsiktligt slutat använda några avancerade tekniker för att inte avslöja mina otillräckliga grundkunskaper. Under följande år gick jag igenom en mödosam process för att återuppbygga min felaktiga, självlärda, baskunskap från grunden. Jag är tacksam för att Kate-gruppen ändå behöll versionen på sin webbsida under många år.

### 2020 - Den nya ikonen för Kate

!2020 - Den nya ikonen för Kate](/images/mascot/history/kate-3000px.png)

> Den nya ikonen för Kate var enda gången jag försökte med riktig vektorgrafik. Det var vid den tidpunkten jag började utveckla en svag konstnärlig känsla. Jag hade ingen tidigare erfarenhet av ikondesign, och det fanns ingen disciplin såsom proportioner baserade på matematik. Varje form och kurva justerades med användning av min konstnärliga instinkt. Konceptet var mycket enkelt: det är en fågel formad som bokstaven "K", och den måste vara skarp. Fågeln var tjockare tills Christoph påpekade det, och jag omvandlade den senare till en smidigare form.

### 2021 - Utsträckning av maskoten

![Version från 2021 - Skiss av den nya maskoten](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> I den här inledande skissen, var jag frestad att överge det robotliknande konceptet och istället bara rita en gullig fågel som såg bra ut.

### 2021 - Design av maskoten

![Version från 2021 - Omdesign av den nya maskoten](/images/mascot/history/electrichearts_20210103_kate_design.png)

> Omdesignen av Kates maskot återupptogs efter att jag var klar med att rita den nya startskärmen för kommande Krita 5.0. Jag lyckades pressa mig själv till att göra mer varje gång jag försökte med en ny bild vid tillfället, så jag var fast besluten att behålla det robotliknande konceptet. Även om proportionen var fel, så var de mekaniska designelementen mer eller mindre myntade i den här.

### 2021 - Slutgiltig maskot

![Version från 2021 - Slutlig version av hackspetten Kate](/images/mascot/electrichearts_20210103_kate_normal.png)

> Proportionen justerades efter att Christoph hade påpekat det. Placeringen justerades så att den kändes mer komfortabel. Vingarna ritades om efter att ha studerat riktiga fågelvingar. Ett snyggt detaljerat tangentbord handritades för att betona programmets huvudanvändning. En abstrakt bakgrund lades till med en riktig tvådimensionell typografisk design. Den allmänna idén är: otvetydigt den allra senaste och artificiell, men också otvetydigt fylld av liv och mänsklighet.
