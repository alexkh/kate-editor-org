---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: Özellikler
---
## Uygulama Özellikleri

![Screenshot showing Kate window splitting feature and search & replace plugin](/images/kate-window.webp)

+ Pencereyi yatay ve dikey olarak bölerek aynı anda birden çok belgeyi düzenleyin
+ Tonla eklenti: [Gömülü uçbirim](https://konsole.kde.org), SQL eklentisi, yapı eklentisi, GDB eklentisi, Dosyalarda Değiştir, ve çok daha fazlası
+ Çok belgeli arabirim (MDI)
+ Oturum desteği

## Genel Özellikler

![Kate bul ve değiştir özelliğini gösteren ekran görüntüsü](/images/kate-search-replace.png)

+ Kodlama desteği (Unicode ve çok daha fazlası)
+ İki yönlü metin oluşturma desteği
+ Otomatik algılamayı da içeren satır sonu desteği (Windows, Unix, Mac)
+ Ağ saydamlığı (uzak dosyaları açma)
+ Betik yazımı ile genişletilebilir

## Gelişmiş Düzenleyici Özellikleri

![Satır numarası ve yer imi ile Kate kenarlığı](/images/kate-border.png)

+ Bookmarking system (also supported: break points etc.)
+ Scroll bar marks
+ Line modification indicators
+ Line numbers
+ Code folding

## Sözdizim Vurgulama

![Screenshot of Kate syntax highlighting features](/images/kate-syntax.png)

+ Highlighting support for over 300 languages
+ Bracket matching
+ Smart on-the-fly spell checking
+ Highlighting of selected words

## Programming Features

![Screenshot of Kate programming features](/images/kate-programming.png)

+ Scriptable auto indentation
+ Smart comment and uncomment handling
+ Auto completion with argument hints
+ Vi input mode
+ Rectangular block selection mode

## Search & Replace

![Screenshot of Kate incremental search feature](/images/kate-search.png)

+ Incremental search, also known as &#8220;find as you type&#8221;
+ Support for multiline search & replace
+ Regular expression support
+ Search & replace in multiple opened files or files on disk

## Backup and Restore

![Screenshot of Kate recover from crash feature](/images/kate-crash.png)

+ Backups on save
+ Swap files to recover data on system crash
+ Undo / redo system
