---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: Możliwości
---
## Możliwości programu

![Zrzut ekranu pokazujący możliwość podziału okna Kate oraz wtyczka szukania i zastępowania](/images/kate-window.webp)

+ Oglądaj i edytuj wiele dokumentów w tym samym czasie, dzieląc okno w poziomie lub pionie
+ Wiele wtyczek: [osadzony terminal](https://konsole.kde.org), wtyczka SQL, wtyczka GDB, Zastąp w plikach oraz więcej
+ Interfejs wielodokumentowy (MDI)
+ Obsługa sesji

## Ogólne możliwości

![Zrzut ekranu pokazujący możliwość szukania i zastępowania Kate](/images/kate-search-replace.png)

+ Obsługa kodowania (Unikod i wiele innych)
+ Obsługa dwukierunkowego wyświetlania tekstu
+ Obsługa zakończeń wierszy (Windows, Unix, Mac), uwzględniając ich wykrywanie
+ Przezroczystość sieciowa (otwieraj zdaln pliki)
+ Rozszerzalne przez skrypty

## Rozszerzone możliwości edytora

![Zrzut ekranu Kate pokazujący ramkę z numerami wierszy i zakładkami](/images/kate-border.png)

+ System zakładek (obsługiwane są również pułapki itp.)
+ Znaczniki na pasku przewijania
+ Wskaźniki zmiany wiersza
+ Numery wierszy
+ Zwijanie kodu

## Podświetlanie składni

![Zrzut ekranu Kate pokazujący możliwości podświetlania składni](/images/kate-syntax.png)

+ Obsługa podświetlania dla ponad 300 języków
+ Podświetlanie naprzeciwległych nawiasów
+ Mądre sprawdzanie pisowni w locie
+ Podświetlanie zaznaczonych słów

## Możliwości programowania

![Zrzut ekranu Kate pokazujący możliwości programowania](/images/kate-programming.png)

+ Samowcinanie z obsługą skryptów
+ Obsługa mądrego dodawania i usuwania uwag
+ Samouzupełnianie z podpowiadaniem argumentów
+ Tryb wprowadzania Vi
+ Tryb zaznaczania prostokątem

## Znajdź i zamień

![Zrzut ekranu Kate pokazujący możliwości przyrostowego wyszukiwania](/images/kate-search.png)

+ Przyrostowe wyszukiwanie, znane także jako &#8220;szukaj podczas pisania&#8221;
+ Obsługa dla wielowierszowego wyszukiwania i zastępowania
+ Obsługa wyrażeń regularnych
+ Szukaj i zastąp w wielu otwartych plikach na dysku

## Kopia zapasowa i przywracanie

![Zrzut ekranu Kate pokazujący możliwości powrotu z usterki](/images/kate-crash.png)

+ Kopia zapasowa przy zapisie
+ Pliki wymiany do odzyskiwania danych po wsypaniu systemu
+ System wycofywania / ponawiania
