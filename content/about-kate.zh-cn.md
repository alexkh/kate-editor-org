---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: 功能
---
## 应用程序功能

![展示 Kate 分割窗口功能和查找替换插件功能的屏幕截图](/images/kate-window.webp)

+ 水平垂直分割窗口，同时查看编辑多个文档
+ 众多插件：[内嵌终端](https://konsole.kde.org)、SQL 插件、构建插件、GDB 插件、在文件中替换等等
+ 多文档界面 (MDI)
+ 会话支持

## 通用功能

![展示 Kate 搜索与替换功能的屏幕截图](/images/kate-search-replace.png)

+ 编码支持 (Unicode 及许多其他编码)
+ 双向文字渲染支持
+ 换行符支持 (Windows、Unix、Mac)，包括自动检测
+ 网络透明 (可打开远程文件)
+ 可通过脚本扩展

## 高级编辑功能

![展示 Kate 正在显示行号及书签的边框的屏幕截图](/images/kate-border.png)

+ 书签系统 (也支持断点等功能)
+ 滚动条标记
+ 行修改指示器
+ 行号
+ 代码折叠

## 语法高亮

![Kate 语法高亮功能的屏幕截图](/images/kate-syntax.png)

+ 语法高亮支持 300 多种语言
+ 括号匹配
+ 智能实时拼写检查
+ 高亮选中词

## 编程功能

![Kate 编程功能的截图](/images/kate-programming.png)

+ 可编程自动缩进
+ 智能注释与取消注释处理
+ 带参数提示的自动补全
+ Vi 输入模式
+ 矩形方块选择模式

## 搜索和替换

![Kate 增量搜索功能的屏幕截图](/images/kate-search.png)

+ 增量搜索，也称为 &#8220;输入时搜索&#8221;
+ 支持多行搜索及替换
+ 正则表达式支持
+ 在多个打开的文件中或者磁盘上的文件中进行搜索及替换

## 备份与还原

![展示 Kate 崩溃恢复功能的屏幕截图](/images/kate-crash.png)

+ 保存时备份
+ 通过交换文件在系统崩溃时恢复数据
+ 撤销/重做系统
