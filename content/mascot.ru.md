---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: Талиман Kate
---
Талисман приложения Kate — «Кибердятел» — был создан художником [Tyson Tan] (https://www.tysontan.com/).

Текущий вариант нашего талисмана была впервые представлен [в апреле 2021 года](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## Кибердятел Kate

![Кибердятел Kate](/images/mascot/electrichearts_20210103_kate_normal.png)

## Версия без текста внизу изображения

![Кибердятел Kate - без текста](/images/mascot/electrichearts_20210103_kate_notext.png)

## Версия без фона

![Кибердятел Kate - без фона](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Лицензия

Цитата автора талисмана (Tyson Tan) относительно лицензирования:

> Настоящим я дарю проекту «редактор текста Kate» новый талисман — «Кибердятел Kate».Работы имеют двойное лицензирование: LGPL (или подобная лицензия, используемая проектом «редактор текста Kate») и Creative Commons BY-SA. Таким образом, вам не нужно спрашивать у меня разрешения при использовании и/или изменении изображений талисмана.

## История создания талисмана «Кибердятел Kate»

Я спросил Тайсона, могу ли я поделиться промежуточными версиями, которыми мы обменялись, чтобы показать эволюцию, и он согласился и даже написал несколько кратких описаний отдельных шагов. Это включает в себя первоначальный талисман и значок, которые вписываются в эту эволюцию дизайна. Поэтому я просто цитирую то, что он написал о различных шагах ниже.

### 2014 — Кибердятел Kate

![Версия 2014 года - Кибердятел Kate](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> Версия 2014 года была создана мной, вероятно, в то время, когда мои художественные способности были на самом низком уровне. Я не рисовал много лет и был готов полностью забросить это занятие. В этой версии было несколько интересных идей, а именно скобки {} на груди талисмана и набор использованных цветов, на которые меня вдохновила подсветкой кода приложения doxygen. Исполнение было очень упрощённым — в то время я сознательно избегал использовать какие-либо причудливые техники рисования, чтобы не выставить напоказ свой недостаток навыков. В последующие годы мне пришлось пройти нелёгкий путь самостоятельного художественного совершенствования, начавшийся с самого примитивного уровня. Я благодарен команде Kate за то, что она хранила эту версию талисмана на своём сайте в течение многих лет.

### 2020 — Новый значок Kate

![2020 - Новый значок Kate](/images/mascot/history/kate-3000px.png)

> The new Kate icon was the only time I attempted real vector art. It was at that time I began to develop some faint artistic sense. I had no previous experience of icon design, there was zero discipline like math-guided proportion. Every shape and curve was tweaked using my instinct as an artist. The concept was very simple: it's a bird shaped like the letter "K", and it has to be sharp. The bird was chunkier until Christoph pointed it out, I later turned it into a more sleek shape.

### 2021 — Набросок талисмана

![2021 version - Sketch of the new mascot](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> In this initial sketch, I was tempted to ditch the robotic concept and just draw a cute good looking bird instead.

### 2021 - Mascot design

![2021 version - Redesign of the new mascot](/images/mascot/history/electrichearts_20210103_kate_design.png)

> Kate's mascot re-design resumed after I finished drawing the new splash picture for the upcoming Krita 5.0. I was able to push myself to do more each time I attempted a new picture at that time, so I was determined to keep the robotic concept. Although the proportion was off, the mech design elements was more or less coined in this one.

### 2021 — Окончательная версия талисмана

![2021 version - Final Kate the Cyber Woodpecker](/images/mascot/electrichearts_20210103_kate_normal.png)

> The proportion was adjusted after Christoph pointed it out. The pose was adjusted to feel more comfortable. The wings were re-drawn after studying on real bird wings. A cool-looking, detailed keyboard was hand-drawn to emphasize the main usage of the application. An abstract background was added with a proper 2D typography design. The general idea is: unmistakably cutting edge and artificial, but also unmistakably full of life and humanity.
