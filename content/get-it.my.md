---
author: ခရစ်စ်တိုကူမန်း
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: ကိတ်ကိုရယူမည်
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### လင်းနပ်စ် နှင့် ယူနစ်စ်များ

+ [သင်၏ဒစ်စထရီဗျူးရှင်း](https://kde.org/distributions/) မှ [ကိတ်](https://apps.kde.org/en/kate) သို့မဟုတ် [ကေရိုက်](https://apps.kde.org/en/kwrite) တပ်ဆင်မည်

{{< appstream_badges >}}

+ [Kate's Snap package in Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate's release (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Kate's nightly (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Build it](/build-it/#linux) မူရင်းမှ။

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### ဝင်းဒိုးများ

+ [မိုက်ခရိုဆော့ဖ်စတိုးရှိ ကိတ်](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)
+ [Kate release (64bit) installer](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [ကိတ်ညစဥ် (၆၄ဘစ်) တပ်ဆင်ဖိုင်လ်](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Build it](/build-it/#windows) မူရင်းမှ။

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Kate release installer](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Kate nightly installer](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Build it](/build-it/#mac) မူရင်းမှ။

{{< /get-it >}}

{{< get-it-info >}}

**ထုတ်ဝေမှုများအကြောင်း -** <br> [ကိတ်](https://apps.kde.org/en/kate) နှင့် [ကေရိုက်](https://apps.kde.org/en/kwrite) သည် [ကေဒီအီးအပ္ပလီကေးရှင်းများ](https://apps.kde.org)တွင် ပါဝင်သည်။ ယင်းတို့ကို [တစ်နှစ်သုံးကြိမ်အစုလိုက်အပြုံလိုက်ထုတ်ဝေသည်](https://community.kde.org/Schedules)။ [စာသားတည်းဖြတ်ခြင်း](https://api.kde.org/frameworks/ktexteditor/html/) နှင့် [စာအထားအသိုအလင်းတင်ပြခြင်း](https://api.kde.org/frameworks/syntax-highlighting/html/) ဆိုင်ရာအင်ဂျင်များကို [လစဥ်အပ်ဒိတ်တင်သည်](https://community.kde.org/Schedules/Frameworks)ဖြစ်သော [ကေဒီအီးမူဘောင်များ](https://kde.org/announcements/kde-frameworks-5.0/) မှထုတ်ပေးသည်။ ထုတ်ဝေမှုအသစ်များကို [ဤနေရာ](https://kde.org/announcements/) တွင် ကြေညာသည်။

\* ကေဒီအီးမူဘောင်များနှင့် ကိတ်၏နောက်ဆုံးပေါ်ဗာရှင်းတို့သည် **ထုတ်ဝေမှု** ပတ်ကေ့ချ်တွင် ပါဝင်သည်။

\*\* The **ညစဥ်** ပတ်ကေ့ချ်များကို ဇစ်မြစ်ကုတ်မှ နေ့စဥ်ကွန်ပိုင်းလုပ်ထားသည်။ ထို့ကြောင့် ယင်းတို့သည် တည်ငြိမ်မှုမရှိဘဲ ဆော့ဖ်ဝဲလ်ပြစ်ချက်များ သို့မဟုတ် တစ်ဝက်တစ်ပျက် စွမ်းဆောင်ရည်များ ပါဝင်သည်။ ယင်းတို့ကို စမ်းသပ်ရန်အတွက်သာ သုံးရန် အကြုံပြုသည်။

{{< /get-it-info >}}
