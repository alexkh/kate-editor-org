---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Загрузить Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux и UNIX-ы

+ Установите [Kate](https://apps.kde.org/en/kate) или [KWrite](https://apps.kde.org/en/kwrite) из [пакетов вашего дистрибутива](https://kde.org/distributions/)

{{< appstream_badges >}}

+ [Пакет Kate в Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Выпуск Kate (64-бит) в формате пакета AppImage](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Ночная сборка Kate (64-бит) в формате пакета AppImage](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Собрать Kate](/build-it/#linux) из исходников.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate в Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate в Chocolatey](https://chocolatey.org/packages/kate)
+ [Kate release (64bit) installer](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Файл для установки ночной сборки Kate (64-бит)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Собрать Kate](/build-it/#windows) из исходников.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Установщик Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Установщик ночной сборки Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Собрать Kate](/build-it/#mac) из исходников.

{{< /get-it >}}

{{< get-it-info >}}

**О выпусках Kate:** <br> [Kate](https://apps.kde.org/en/kate) и [KWrite](https://apps.kde.org/en/kwrite) являются частью набора [KDE Applications](https://apps.kde.org), который [обычно обновляется 3 раза в год](https://community.kde.org/Schedules). Движки [редактирования текста](https://api.kde.org/frameworks/ktexteditor/html/) и [подсветки синтаксиса](https://api.kde.org/frameworks/syntax-highlighting/html/) являются частью библиотек [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), которые [обновляются ежемесячно](https://community.kde.org/Schedules/Frameworks). О новых выпусках объявляют [здесь](https://kde.org/announcements/).

\* В этих пакетах **сборок** содержатся последние версии Kate и KDE Frameworks.

\*\* **Ночные** сборки автоматически собираются с исходников каждый день, поэтому они могут работать нестабильно, иметь баги или возможности, которые еще не реализованы до конца. Рекомендуется использовать их только с целью тестирования.

{{< /get-it-info >}}
