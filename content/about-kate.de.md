---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: Funktionen
---
## Funktionen der Anwendung

![Screenshot showing Kate window splitting feature and search & replace plugin](/images/kate-window.webp)

+ Gleichzeitiges Anzeigen und Bearbeiten mehrerer Dokumente durch horizontale und vertikale Teilung des Fensters
+ Viele Module: [Eingebettetes Terminal](https://konsole.kde.org), SQL-Modul, Erstellen-Modul, GDB-Modul, Ersetzen in Dateien, und mehr
+ Multi-Dokument-Schnittstelle (MDI)
+ Unterstützung für Sitzungen

## Allgemeine Funktionen

![Bildschirmfoto der Funktion Suchen und Ersetzen in Kate](/images/kate-search-replace.png)

+ Unterstützung für Kodierung (Unicode und viele andere)
+ Bi-Direktionale Anzeige von Schriften
+ Unterstützung für Zeilenenden unter Windows, Unix, Mac, einschließlich automatischer Erkennung
+ Netzwerktransparenz, Öffnen von Dateien auf fremden Rechnern
+ Erweiterbar durch Skripte

## Weitergehende Editor-Funktionen

![Bildschirmfoto vom Rand mit Zeilennummern und Lesezeichen in Kate](/images/kate-border.png)

+ Lesezeichen-System, auch für Haltepunkte usw.
+ Markierungen auf der Bildlaufleiste
+ Kennzeichnung von Änderungen in Textzeilen
+ Zeilennummern
+ Quelltextausblendung

## Syntaxhervorhebung

![Bildschirmfoto von Kate mit Syntaxhervorhebung)](/images/kate-syntax.png)

+ Syntaxhervorhebung für mehr als 300 Sprachen
+ Zusammengehörige Klammern
+ Rechtschreibprüfung während des Schreibens
+ Hervorhebung ausgewählter Worte

## Programmier-Funktionen

![Bildschirmfoto der Programmier-Funktionen in Kate](/images/kate-programming.png)

+ Automatische Einrückung, durch Skripte steuerbar
+ Intelligente Möglichkeiten, um Quelltext als Kommentar zu markieren und dies wieder aufzuheben
+ Automatisch Vervollständigung mit Hinweisen zu Argumenten
+ VI-Eingabemodus
+ Rechteck-Blockauswahlmodus

## Suchen & Ersetzen

![Bildschirmfoto der inkrementellen Suche in Kate](/images/kate-search.png)

+ Inkrementelle Suche, auch &#8220;Suchen bei der Eingabe&#8221; genannt
+ Unterstützung für Suchen & Ersetzen in mehreren Zeilen
+ Unterstützung für reguläre Ausdrücke
+ Suchen und Ersetzen in mehreren geöffneten Dateien oder in Dateien auf der Festplatte

## Sichern und Wiederherstellen

![Bildschirmfoto von Kate beim Wiederherstellen nach einem Absturz](/images/kate-crash.png)

+ Sicherungskopien beim Speichern
+ Swap-Dateien, um nach einem Systemabsturz Daten wiederherzustellen
+ System für Rückgängigmachen / Wiederherstellen
