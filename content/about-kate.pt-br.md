---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: Recursos
---
## Recursos do aplicativo

![Captura de tela do recurso de dividir janelas do Kate e o plugin de pesquisa e substituição](/images/kate-window.webp)

+ Visualize e edite múltiplos documentos ao mesmo tempo, dividindo a janela horizontalmente e verticalmente
+ Vários plugins: [Terminal embutido](https://konsole.kde.org), plugin do SQL, plugin de build, plugin do GDB, Pesquisar e substituir e mais
+ Interface multi-documentos (MDI)
+ Suporte à sessão

## Recursos gerais

![Captura de tela do recurso de pesquisar e substituir do Kate](/images/kate-search-replace.png)

+ Suporte à codificação (unicode e muitos outros)
+ Suporte à renderização de texto bidirecional
+ Suporte a fim de linha (Windows, Unix, Mac) incluindo detecção automática
+ Transparência de rede (abre arquivos remotos)
+ Extensível através de scripts

## Recursos avançados do editor

![Captura de tela da borda do Kate com o número da linha favoritos](/images/kate-border.png)

+ Sistema de favoritos (também suportada: pontos de quebra, etc)
+ Marcas na barra de rolagem
+ Indicadores de modificação de linhas
+ Números de linha
+ Dobradura de código

## Realce de sintaxe

![Captura de tela do recurso de realce de sintaxe do Kate](/images/kate-syntax.png)

+ Suporte a realce para mais de 300 linguagens
+ Correspondência de parênteses
+ Verificação ortográfica instantânea e inteligente
+ Realce de palavras selecionadas

## Recursos de programação

![Captura de tela dos recursos de programação do Kate](/images/kate-programming.png)

+ Recuo automático programável
+ Tratamento de comentar e descomentar inteligente
+ Complementação automática com dicas de argumentos
+ Modo de entrada Vi
+ Modo de seleção de bloco retangular

## Pesquisar e substituir

![Captura de tela do recurso de pesquisa incremental do Kate](/images/kate-search.png)

+ Pesquisa incremental, também conhecida como &#8220;encontre enquanto digita&#8221;
+ Suporte a pesquisa e substituição multilinhas
+ Suporte à expressão regular
+ Pesquisar e substituir em múltiplos arquivos abertos ou arquivos no disco

## Backup e restauração

![Captura de tela do recurso de recuperar de uma quebra do Kate](/images/kate-crash.png)

+ Backups ao salvar
+ Arquivos de swap para recuperar dados na quebra do sistema
+ Sistema de fazer/refazer
