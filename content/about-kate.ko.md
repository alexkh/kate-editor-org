---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: 기능
---
## 프로그램 기능

![Kate 창 나누기 기능과 찾아 바꾸기 플러그인 스크린샷](/images/kate-window.webp)

+ 창을 수직과 수평으로 나눠서 여러 문서를 한 번에 보고 편집하기
+ 다양한 플러그인: [내장된 터미널](https://konsole.kde.org), SQL 플러그인, 빌드 플러그인, GDB 플러그인, 파일에서 찾아 바꾸기 등
+ 다중 문서 인터페이스(MDI) 
+ 세션 지원

## 일반 기능

![Kate 찾아 바꾸기 기능 스크린샷](/images/kate-search-replace.png)

+ 인코딩 지원(유니코드 및 기타 지역 인코딩)
+ 양방향 텍스트 렌더링 지원
+ 줄 끝 문자 지원(Windows, UNIX, Mac), 자동 감지 포함
+ 네트워크 투명성(원격 파일 열기)
+ 스크립트로 확장 가능

## 고급 편집기 기능

![줄 번호와 책갈피를 표시하는 Kate 창 모서리 부분 스크린샷](/images/kate-border.png)

+ 책갈피 시스템(중단점 등도 지원)
+ 스크롤 바 마커
+ 줄 수정 표시기
+ 줄 번호
+ 코드 접기

## 구문 강조

![Kate 구문 강조 기능 스크린샷](/images/kate-syntax.png)

+ 300개 이상 언어의 구문 강조
+ 괄호 일치
+ 똑똑한 즉석 맞춤법 검사
+ 선택한 단어 강조

## 프로그래밍 기능

![Kate 프로그래밍 기능 스크린샷](/images/kate-programming.png)

+ 스크립트 가능한 자동 들여쓰기
+ 똑똑한 주석과 주석 해제 처리
+ 인자 힌트를 제공하는 자동 완성
+ VI 입력 모드
+ 사각형 블록 선택 모드

## 찾아 바꾸기

![Kate 증분 검색 스크린샷](/images/kate-search.png)

+ 증분 검색, &#8220;입력한 대로 검색&#8221;이라고도 함
+ 여러 줄에서 찾기 및 바꾸기 지원
+ 정규 표현식 지원
+ 열린 파일 여러 개 및 디스크에 있는 파일에서 찾아 바꾸기

## 백업과 복원

![Kate 충돌 복구 스크린샷](/images/kate-crash.png)

+ 저장할 때 백업 생성
+ 스왑 파일을 통한 시스템 충돌 시 데이터 복구
+ 실행 취소/다시 실행
