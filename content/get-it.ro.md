---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Obține Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unices

+ Install [Kate](https://apps.kde.org/en/kate) or [KWrite](https://apps.kde.org/en/kwrite) from [your distribution](https://kde.org/distributions/)

{{< appstream_badges >}}

+ [Kate's Snap package in Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate's release (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Kate's nightly (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Build it](/build-it/#linux) from source.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate in the Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)
+ [Kate release (64bit) installer](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Kate nightly (64bit) installer](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Build it](/build-it/#windows) from source.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Kate release installer](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Kate nightly installer](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Build it](/build-it/#mac) from source.

{{< /get-it >}}

{{< get-it-info >}}

**About the releases:** <br> [Kate](https://apps.kde.org/en/kate) and [KWrite](https://apps.kde.org/en/kwrite) are part of [KDE Applications](https://apps.kde.org), which are [released typically 3 times a year en-masse](https://community.kde.org/Schedules). The [text editing](https://api.kde.org/frameworks/ktexteditor/html/) and the [syntax highlighting](https://api.kde.org/frameworks/syntax-highlighting/html/) engines are provided by [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), which is [updated monthly](https://community.kde.org/Schedules/Frameworks). New releases are announced [here](https://kde.org/announcements/).

\* The **release** packages contain the latest version of Kate and KDE Frameworks.

\*\* The **nightly** packages are automatically compiled daily from source code, therefore they may be unstable and contain bugs or incomplete features. These are only recommended for testing purposes.

{{< /get-it-info >}}
