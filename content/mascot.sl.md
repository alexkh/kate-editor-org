---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: Maskota programa Kate
---
Maskoto programa Kate, Kiber-žolno Kate, je oblikoval [Tyson Tan](https://www.tysontan.com/).

Trenutni razvoj naše maskote je bil prvič razkrit [v aprilu 2021](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## Kiber-žolna Kate

![Kiber-žolna Kate](/images/mascot/electrichearts_20210103_kate_normal.png)

## Različica brez spremnega besedila

![Kiber-žolna Kate - brez besedila](/images/mascot/electrichearts_20210103_kate_notext.png)

## Različica brez ozadja

![Kiber-žolna Kate - brez ozadja](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Licenciranje

Za licenco, če citiramo Tysona:

> S tem podarjam novo Kiber-žolno Kate projektu urejevalnik Kate. Umetniška dela so izdana pod dvojnim dovoljenjem, pod LGPL (ali kako drugače kot urejevalnik Kate) in Creative Commons BY-SA. Na ta način me ni treba prositi za to dovoljenje pred uporabo/spreminjanjem maskote.

## Zgodovina oblikovanja Kiber-žolne Kate

Tysona sem vprašal, ali lahko delim vmesne različice, ki smo si jih izmenjali, da pokažemo razvoj, in strinjal se je ter celo napisal nekaj kratkih povzetkov o posameznih korakih. To vključuje začetno maskoto in ikono, ki ustreza temu razvoju oblikovanja. Zato samo navajam, kar je napisal o različnih korakih spodaj.

### 2014 - Žolna Kate

![Različica 2014 - Žolna Kate](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> Različica iz leta 2014 je nastala morda v času moje najnižje točke kot umetnika. V teh letih nisem risal veliko in sem bil na robu, da se popolnoma odpovem risanju. V tej različici je bilo nekaj zanimivih idej, in sicer {} oklepaji na prsih in barvna shema, ki jo navdihuje shema poudarjanja v barvni shemi kisika. Vendar je bila izvedba zelo poenostavljena - takrat sem namerno prenehal uporabljati kakršne koli tehnike, ki bi razkrile moje neustrezne osnovne spretnosti. V naslednjih letih bi šel skozi naporen proces obnoviti svojo okvarjeno, samouko umetniško podlago od začetka. Hvaležen sem, da je ekipa Kate še vedno obdržala to različico na svoji spletni strani za več let.

### 2020 - Nova ikona Kate

![2020 - nova ikona Kate](/images/mascot/history/kate-3000px.png)

> Nova ikona Kate je bila moj edini poskus prave vektorske slike. Bilo je ob takrat sem začel razvijati nekaj šibkega umetniškega čuta. Prej še nisem imel izkušenj pri oblikovanju ikon, nisem poznal discipline, kot so matematično vodeni proporci. Vsaka oblika in krivulja je bila prilagojena z uporabo mojega umetniškega instinkta. Koncept je bil zelo preprost: gre za ptico v obliki črke "K", ki mora biti oster. Ptica je bila mogočnejša, dokler je Christoph ni usmeril ven, sem jo kasneje spremenil v bolj gladko obliko.

### 2021 - Skica maskote

![2021 verzija - Skica nove maskote](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> V tej začetni skici me je zamikalo, da bi se odrekel robotskemu konceptu in namesto tega narisal zgolj prikupno lepo ptico.

### 2021 - Oblikovanje maskote

![Različica 2021 - Ponovno oblikovanje nove maskote](/images/mascot/history/electrichearts_20210103_kate_design.png)

> Preoblikovanje maskote Kate se je spet nadaljevalo, ko sem končal z risanjem novega slike dobrodošlice za prihajajoči program Krita 5.0. Lahko sem se potrudil, da naredim vsakič več, ko sem takrat poskusil novo sliko, sem bil odločen ohraniti robotski koncept. Čeprav so bili proporci neomejeni, je bila mehanska zasnova elementov bolj ali manj skovana v tem.

### 2021 - Končna maskota

![Različica 2021 - Končna Kiber-žolna Kate](/images/mascot/electrichearts_20210103_kate_normal.png)

> Proporci so bili prilagojen, ko je na to opozoril Christoph. Poza je bila prilagojena, da se počuti bolj udobno. Krila so bila na novo narisana po preučevanju pravih ptičjih kril. Ročno je bila narisana čudovita, podrobna tipkovnica, dapoudari glavno uporabo aplikacije. Abstraktno ozadje je bilo dodano z ustreznim dizajnom 2D tipografije. Splošna ideja je: nedvomno vrhunska in umetna, a tudi nedvomno polna življenja in človečnosti.
