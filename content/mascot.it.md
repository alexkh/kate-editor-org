---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: La mascotte di Kate
---
La mascotte di Kate, Kate il picchio cibernetico, è stata disegnata da [Tyson Tan](https://www.tysontan.com/).

L'evoluzione corrente della nostra mascotte è stata svelata per la prima volta [nell'aprile del 2021](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## Kate, il picchio cibernetico

![Kate, il picchio cibernetico](/images/mascot/electrichearts_20210103_kate_normal.png)

## Versione senza testo a piè di pagina

![Kate, il picchio cibernetico - senza testo](/images/mascot/electrichearts_20210103_kate_notext.png)

## Versione senza immagine di sfondo

![Kate, il picchio cibernetico - senza immagine di sfondo](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Licenza

Per la licenza, per citare Tyson:

> Con la presente dono la nuova mascotte di Kate, il picchio cibernetico, al progetto Kate Editor. Le opere sono rilasciate con una doppia licenza, LGPL (o comunque la stessa di Kate Editor) e Creative Commons BY-SA. In questo modo non c'è bisogno di chiedermi il permesso per utilizzare o per modificare la mascotte.

## Cronologia del disegno di Kate, il picchio cibernetico

Ho chiesto a Tyson il permesso di condividere le versioni intermedie che ci siamo scambiati per mostrarne l'evoluzione; me l'ha concesso e ha persino scritto alcune brevi note sui singoli passaggi. Queste includono la mascotte iniziale e l'icona che si adattano a questa evoluzione del disegno. Pertanto cito solo quello che ha scritto sui diversi passaggi qui sotto.

### 2014 - Kate il picchio

![Versione del 2014 - Kate il picchio](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> La versione del 2014 è stata realizzata forse nel punto più basso come artista: non disegnavo molto da anni, ed ero sul punto di mollare. C'erano però alcune idee interessanti in questa versione, ad esempio le parentesi {} sul petto e lo schema di colori ispirato all'evidenziazione del codice doxygen, tuttavia, l'esecuzione era molto semplicistica: in quel momento ho smesso deliberatamente di usare qualsiasi tecnica di fantasia per esporre le mie inadeguate abilità di base. Negli anni seguenti sarei passato attraverso ad un arduo processo di ricostruzione da zero delle mie basi di artistiche da autodidatta. Sono grato alla squadra di Kate di aver mantenuto questo disegno per molti anni sul loro sito web.

### 2020 - la nuova icona di Kate

![2020 - la nuova icona di Kate](/images/mascot/history/kate-3000px.png)

> La nuova icona di Kate è stata l'unica volta in cui ho provato la vera arte vettoriale: è stato in quel momento che ho iniziato a sviluppare un debole senso artistico. Non avevo alcuna precedente esperienza nella progettazione di icone, e non c'era una disciplina come le proporzioni guidate dalla matematica. Ogni forma e ogni curva è stata modificata usando il mio istinto di artista. Il concetto era molto semplice: è un uccello a forma di lettera «K», e deve essere appuntito. L'uccello era più grosso fino a quando Christoph non lo ha fatto notare, così in seguito gli ho dato una forma più elegante.

### 2021 - schizzo della mascotte

![Versione del 2021 - schizzo della nuova mascotte](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> In questo schizzo iniziale ero tentato di abbandonare il concetto robotico, disegnando semplicemente un simpatico uccellino di bell'aspetto.

### 2021 - disegno della mascotte

![Versione del 2021 - nuovo disegno della nuova mascotte](/images/mascot/history/electrichearts_20210103_kate_design.png)

> La modifica della mascotte di Kate è ripresa una volta finito di disegnare la nuova schermata iniziale dell'imminente Krita 5.0. In quel momento sono stato capace di spingermi a fare di più in ogni tentativo di disegnare una nuova immagine, quindi ero determinato a mantenere il concetto robotico. Sebbene le proporzione fossero sbagliate, gli elementi meccanici del disegno sono stati più o meno coniati in questa versione.

### 2021 - la mascotte finale

![Versione del 2021 - versione finale di Kate il picchio cibernetico](/images/mascot/electrichearts_20210103_kate_normal.png)

> Le proporzioni sono state sistemate quando Christoph me l'ha fatto notare, la posa è stata modificata per farla sembrare più adeguata, le ali sono state ridisegnate dopo aver studiato le ali di un vero uccello, una tastiera dall'aspetto, accattivante e dettagliata è stata disegnata a mano per enfatizzare l'utilizzo principale dell'applicazione ed è stato aggiunto uno sfondo astratto con un design tipografico 2D appropriato. L'idea generale è: inconfondibilmente all'avanguardia e artificiale, ma anche inconfondibilmente pieno di vita e di umanità.
