---
title: Merge Requests - February 2020
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2020/02/
---

#### Week 09

- **Kate - [Session manager: empty session list](https://invent.kde.org/utilities/kate/-/merge_requests/70)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Prefer data-error/warning/information icons over dialog-* for line marker](https://invent.kde.org/utilities/kate/-/merge_requests/69)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 6 days.

#### Week 08

- **Kate - [search: handle both optional Project search options equally](https://invent.kde.org/utilities/kate/-/merge_requests/66)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

#### Week 06

- **Kate - [#417328 Properly declare supported file types](https://invent.kde.org/utilities/kate/-/merge_requests/64)**<br />
Request authored by [Jan Przybylak](https://invent.kde.org/janpr) and merged at creation day.

- **Kate - [[BackTrace Plugin] QRegExp to QRegularExpression](https://invent.kde.org/utilities/kate/-/merge_requests/63)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **Kate - [KUserFeedback integration](https://invent.kde.org/utilities/kate/-/merge_requests/60)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 14 days.

#### Week 05

- **Kate - [Fix usage of QtSingleApplication on Windows and Apple](https://invent.kde.org/utilities/kate/-/merge_requests/61)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

