---
title: Merge Requests - December 2020
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2020/12/
---

#### Week 53

- **Kate - [Update replace display results to match the search results](https://invent.kde.org/utilities/kate/-/merge_requests/156)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [External Tools: Trim executable, name and command name](https://invent.kde.org/utilities/kate/-/merge_requests/155)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [External Tools: Track mimetype changes to enable/disable actions](https://invent.kde.org/utilities/kate/-/merge_requests/154)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Improve search performance by reducing the amount of work](https://invent.kde.org/utilities/kate/-/merge_requests/153)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Variable expansion: Add support for %{Document:Variable:&lt;name&gt;}](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/55)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after 2 days.

- **KSyntaxHighlighting - [C++ Highlighting: QOverload and co.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/139)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [Fix labels beginning with period not being highlighted in GAS](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/137)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Show the line text in search results instead of line no &amp; col](https://invent.kde.org/utilities/kate/-/merge_requests/149)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Changed Go Language Server](https://invent.kde.org/utilities/kate/-/merge_requests/150)**<br />
Request authored by [Marvin Ahlgrimm](https://invent.kde.org/treagod) and merged after 2 days.

- **KSyntaxHighlighting - [C++ highlighting: add qGuiApp macro](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/138)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [Improve dracula theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/136)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Bash, Zsh: ! with if, while, until ; Bash: pattern style for ${var,patt} and ${var^patt}](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/135)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [LSP: Add support for textDocument/implementation](https://invent.kde.org/utilities/kate/-/merge_requests/152)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add documetnation about external tools plugin](https://invent.kde.org/utilities/kate/-/merge_requests/151)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **KTextEditor - [Show the dragged text when dragging](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/54)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [fix #5: Bash, Zsh: comments within array](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/134)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Cucumber feature syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/133)**<br />
Request authored by [Samu Voutilainen](https://invent.kde.org/svoutilainen) and merged at creation day.

- **KSyntaxHighlighting - [Zsh: fix brace expansion in a command](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/131)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

#### Week 52

- **Kate - [Add quick open go-to symbol support in CTags plugin](https://invent.kde.org/utilities/kate/-/merge_requests/145)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Highlight documentation: new attributes for some rules: weakDeliminator and additionalDeliminator](https://invent.kde.org/utilities/kate/-/merge_requests/147)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [add weakDeliminator and additionalDeliminator with keyword, WordDetect, Int,...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/111)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 29 days.

- **KSyntaxHighlighting - [some fixes for Bash many fixes and improvements for Zsh](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/129)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Indexer: reset currentKeywords and currentContext when opening a new definition](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/130)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KTextEditor - [Fix detach in TextRange::fixLookup()](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/51)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Don&#x27;t highlight currentLine if there is an overlapping selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/50)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Update Monokai theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/127)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Verify the correctness/presence of custom-styles in themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/128)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Add Inline Color Picker Plugin](https://invent.kde.org/utilities/kate/-/merge_requests/133)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 10 days.

- **KSyntaxHighlighting - [Add GitHub Dark/Light themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/126)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 51

- **Kate - [Add StartupWMClass parameters to desktop files](https://invent.kde.org/utilities/kate/-/merge_requests/144)**<br />
Request authored by [Markus S.](https://invent.kde.org/markuss) and merged at creation day.

- **KTextEditor - [KateRegExpSearch: fix logic when adding &#x27;\n&#x27; between range lines](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/49)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **KSyntaxHighlighting - [Fix: Backport changes by Andreas Gratzer in MR!113 to SPDX syntax template](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/119)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 13 days.

- **KSyntaxHighlighting - [Add Atom One dark/light themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/125)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Fix monokai attribute &amp; operator color](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/124)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [improve replace speed](https://invent.kde.org/utilities/kate/-/merge_requests/142)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Add Monokai color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/123)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Do not set deprecated CustomName property in knsrc file](https://invent.kde.org/utilities/kate/-/merge_requests/141)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after one day.

- **KSyntaxHighlighting - [CMake: Add missed 3.19 variables and some new added in 3.19.2](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/122)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KTextEditor - [add an action to trigger copy &amp; paste as one action](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/48)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **Kate - [Select first item in quick-open if only one item in list](https://invent.kde.org/utilities/kate/-/merge_requests/139)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add item to tree after setting attributes](https://invent.kde.org/utilities/kate/-/merge_requests/138)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Improve search performance by a huge margin](https://invent.kde.org/utilities/kate/-/merge_requests/136)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix some clazy warnings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/46)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [feat: add text-wrap action icon for Dynamic Word Wrap](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/47)**<br />
Request authored by [Gary Wang](https://invent.kde.org/garywang) and merged at creation day.

- **Kate - [Make Ctrl+W shortcut work in Ctrl+Tab tabswitcher](https://invent.kde.org/utilities/kate/-/merge_requests/135)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 50

- **KTextEditor - [Undo indent in one step](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/45)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [Pass parent to Q*Layout ctor instead of calling setLayout()](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/44)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 9 days.

- **KSyntaxHighlighting - [Improvements of Java, Scala, Kotlin and Groovy](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/121)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Remove unused include](https://invent.kde.org/utilities/kate/-/merge_requests/132)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KSyntaxHighlighting - [fix #5: &amp;&amp; and || in a subcontext and fix function name pattern](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/120)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [add capture attribute to the RegExpr rule. capture=0 (default) disables capture groups](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/118)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 4 days.

#### Week 49

- **KSyntaxHighlighting - [Bash: add (...), ||, &amp;&amp; in [[ ... ]] ; add backquote in [ ... ] and [[ ... ]]](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/117)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [lspclient: switch to a maintained javascript language server](https://invent.kde.org/utilities/kate/-/merge_requests/131)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **KSyntaxHighlighting - [fix dependencies of the generated files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/116)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [indexer: load all xml files in memory to simplify and fix the checkers](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/107)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 11 days.

- **KSyntaxHighlighting - [SPDX comments: fix broken highlighting for multi-line comments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/113)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged after one day.

- **KSyntaxHighlighting - [relaunch syntax generators when the source file is modified](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/114)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [C++ highlighting: update to Qt 5.15](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/115)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

