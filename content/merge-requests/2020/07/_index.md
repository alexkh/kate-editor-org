---
title: Merge Requests - July 2020
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2020/07/
---

#### Week 29

- **KSyntaxHighlighting - [replace some RegExp by AnyChar, DetectChar, Detect2Chars or StringDetect](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/13)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 7 days.

- **Kate - [Highlight documentation: add feature of comment position](https://invent.kde.org/utilities/kate/-/merge_requests/94)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after one day.

- **KSyntaxHighlighting - [language.xsd: remove HlCFloat and introduce char type](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/11)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 6 days.

- **KSyntaxHighlighting - [KConfig: fix $(...) and operators + some improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/14)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [ISO C++: fix performance in highlighting numbers](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/10)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 13 days.

- **KSyntaxHighlighting - [replace RegExpr=[.]{1,1} by DetectChar](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/12)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 4 days.

- **KSyntaxHighlighting - [Lua: attribute with Lua54 and some other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/9)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 13 days.

#### Week 28

- **Kate - [Update D LSP Server](https://invent.kde.org/utilities/kate/-/merge_requests/93)**<br />
Request authored by [Ernesto Castellotti](https://invent.kde.org/ernytech) and merged at creation day.

- **Kate - [Fix error if executable has spaces](https://invent.kde.org/utilities/kate/-/merge_requests/92)**<br />
Request authored by [Ernesto Castellotti](https://invent.kde.org/ernytech) and merged after one day.

#### Week 27

- **Kate - [kate: set last active view as current when restoring viewspace](https://invent.kde.org/utilities/kate/-/merge_requests/91)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

