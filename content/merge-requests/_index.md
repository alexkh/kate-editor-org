---
title: Merge Requests
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
menu:
  main:
    weight: 100
    parent: menu
---

This pages provides an overview of the merge requests we are current working on or did already accepted for the Kate, KTextEditor, KSyntaxHighlighting and kate-editor.org repositories.

## Currently Open Merge Requests
Our team is still working on the following requests. Feel free to take a look and help out, if any of them is interesting for you!
### Kate

- **[Replace Make Persistent with something better](https://invent.kde.org/utilities/kate/-/merge_requests/835)**<br />
Request authored by [loh.tar](https://invent.kde.org/lohtar).

- **[Draft: Fix showing document prefix in document switcher](https://invent.kde.org/utilities/kate/-/merge_requests/860)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov).

- **[remove session applet](https://invent.kde.org/utilities/kate/-/merge_requests/849)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).

- **[Add actions to toggle visibilty of toolviews by sidebar](https://invent.kde.org/utilities/kate/-/merge_requests/834)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).

- **[windows: add copy of Kate icon for unplating](https://invent.kde.org/utilities/kate/-/merge_requests/804)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric).

- **[Use KService and ApplicationLauncherJob for diff tool handling](https://invent.kde.org/utilities/kate/-/merge_requests/801)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella).

- **[Draft: CMakeTools Plugin Improvement](https://invent.kde.org/utilities/kate/-/merge_requests/792)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric).

- **[Add closed files to recent files](https://invent.kde.org/utilities/kate/-/merge_requests/750)**<br />
Request authored by [Raphaël Jakse](https://invent.kde.org/jakse).

- **[rfc support extern diff tool](https://invent.kde.org/utilities/kate/-/merge_requests/737)**<br />
Request authored by [loh.tar](https://invent.kde.org/lohtar).

- **[Use KPluginWidget to manage widgets](https://invent.kde.org/utilities/kate/-/merge_requests/736)**<br />
Request authored by [Tomaz  Canabrava](https://invent.kde.org/tcanabrava).

### KTextEditor

- **[Add FreeBSD Qt6 CI support](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/410)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent).

- **[Enable &#x27;indent text on paste&#x27; by default](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/210)**<br />
Request authored by [Bharadwaj Raju](https://invent.kde.org/bharadwaj-raju).

- **[extend the list of default styles](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/387)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff).

### KSyntaxHighlighting

- **[extend the list of default text styles](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/320)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff).

- **[[WIP] use of several contexts in parallel](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/291)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen).


## Overall Accepted Merge Requests
- 753 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 378 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 328 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 34 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



## Accepted Merge Requests of 2022

- 273 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 152 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 52 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 2 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [August 2022](/merge-requests/2022/08/) (80 requests)
* [July 2022](/merge-requests/2022/07/) (43 requests)
* [June 2022](/merge-requests/2022/06/) (39 requests)
* [May 2022](/merge-requests/2022/05/) (37 requests)
* [April 2022](/merge-requests/2022/04/) (34 requests)
* [March 2022](/merge-requests/2022/03/) (91 requests)
* [February 2022](/merge-requests/2022/02/) (89 requests)
* [January 2022](/merge-requests/2022/01/) (66 requests)


## Accepted Merge Requests of 2021

- 348 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 177 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 144 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 24 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2021](/merge-requests/2021/12/) (51 requests)
* [November 2021](/merge-requests/2021/11/) (32 requests)
* [October 2021](/merge-requests/2021/10/) (45 requests)
* [September 2021](/merge-requests/2021/09/) (15 requests)
* [August 2021](/merge-requests/2021/08/) (34 requests)
* [July 2021](/merge-requests/2021/07/) (45 requests)
* [June 2021](/merge-requests/2021/06/) (57 requests)
* [May 2021](/merge-requests/2021/05/) (57 requests)
* [April 2021](/merge-requests/2021/04/) (37 requests)
* [March 2021](/merge-requests/2021/03/) (91 requests)
* [February 2021](/merge-requests/2021/02/) (109 requests)
* [January 2021](/merge-requests/2021/01/) (120 requests)


## Accepted Merge Requests of 2020

- 87 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 49 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 132 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 2 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2020](/merge-requests/2020/12/) (58 requests)
* [November 2020](/merge-requests/2020/11/) (28 requests)
* [October 2020](/merge-requests/2020/10/) (36 requests)
* [September 2020](/merge-requests/2020/09/) (42 requests)
* [August 2020](/merge-requests/2020/08/) (56 requests)
* [July 2020](/merge-requests/2020/07/) (10 requests)
* [June 2020](/merge-requests/2020/06/) (8 requests)
* [May 2020](/merge-requests/2020/05/) (14 requests)
* [April 2020](/merge-requests/2020/04/) (2 requests)
* [March 2020](/merge-requests/2020/03/) (4 requests)
* [February 2020](/merge-requests/2020/02/) (7 requests)
* [January 2020](/merge-requests/2020/01/) (5 requests)


## Accepted Merge Requests of 2019

- 45 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 6 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



### Monthly Statistics

* [December 2019](/merge-requests/2019/12/) (7 requests)
* [November 2019](/merge-requests/2019/11/) (3 requests)
* [October 2019](/merge-requests/2019/10/) (9 requests)
* [September 2019](/merge-requests/2019/09/) (20 requests)
* [August 2019](/merge-requests/2019/08/) (12 requests)
