---
title: Merge Requests - August 2022
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2022/08/
---

#### Week 34

- **Kate - [Allow files to be DND into the project](https://invent.kde.org/utilities/kate/-/merge_requests/859)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add close all documents action to tab context menu](https://invent.kde.org/utilities/kate/-/merge_requests/863)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [Apply title capitalization to all actions](https://invent.kde.org/utilities/kate/-/merge_requests/862)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [Add tooltip to split view button](https://invent.kde.org/utilities/kate/-/merge_requests/858)**<br />
Request authored by [Jan Bidler](https://invent.kde.org/miepee) and merged at creation day.

- **Kate - [Handle an increase in tab limit](https://invent.kde.org/utilities/kate/-/merge_requests/861)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **Kate - [Improve handling of reducing the tab limit](https://invent.kde.org/utilities/kate/-/merge_requests/857)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **KTextEditor - [Apply title capitalization to all actions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/411)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [[Tabbar] Show tooltips for untitled documents](https://invent.kde.org/utilities/kate/-/merge_requests/856)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **Kate - [Revert &quot;kateprojectpluginview: add us to gui before creating tool views&quot;](https://invent.kde.org/utilities/kate/-/merge_requests/854)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KTextEditor - [Improve singleline selection commenting](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/409)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Change cpp singleLineComments to be AfterWhiteSpace](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/341)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 7 days.

- **KSyntaxHighlighting - [Add FreeBSD Qt6 CI support](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/347)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged at creation day.

- **Kate - [Fix crash when there are no search results](https://invent.kde.org/utilities/kate/-/merge_requests/853)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **Kate - [Add Metals for Scala to the default LSP list](https://invent.kde.org/utilities/kate/-/merge_requests/852)**<br />
Request authored by [Quinten Kock](https://invent.kde.org/colonelphantom) and merged at creation day.

- **Kate - [nicer caption for full path output of local files](https://invent.kde.org/utilities/kate/-/merge_requests/851)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Don&#x27;t search for KF5Activities on Windows/Mac](https://invent.kde.org/utilities/kate/-/merge_requests/850)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **Kate - [Fix segfault when starting up from the session chooser](https://invent.kde.org/utilities/kate/-/merge_requests/847)**<br />
Request authored by [Georg Gadinger](https://invent.kde.org/ggadinger) and merged at creation day.

- **Kate - [add default LSP config for Ruby](https://invent.kde.org/utilities/kate/-/merge_requests/848)**<br />
Request authored by [Georg Gadinger](https://invent.kde.org/ggadinger) and merged at creation day.

- **KTextEditor - [Revert changes to JS API](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/408)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KSyntaxHighlighting - [Indexer: ignore attribute with lookAhead in suggestRuleMerger() and check...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/346)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

#### Week 33

- **Kate - [always follow the user setting for eliding tabs](https://invent.kde.org/utilities/kate/-/merge_requests/846)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Show diagnostic code in UI](https://invent.kde.org/utilities/kate/-/merge_requests/839)**<br />
Request authored by [Glebs Ivanovskis](https://invent.kde.org/i-ky) and merged after 5 days.

- **Kate - [Move KF5DBusAddons dependency into kate/kwrite](https://invent.kde.org/utilities/kate/-/merge_requests/845)**<br />
Request authored by [Andreas Sturmlechner](https://invent.kde.org/asturmlechner) and merged after one day.

- **Kate - [Re-add KF5TextWidgets dependency used by lib](https://invent.kde.org/utilities/kate/-/merge_requests/844)**<br />
Request authored by [Andreas Sturmlechner](https://invent.kde.org/asturmlechner) and merged after one day.

- **KTextEditor - [Add clipboard history dialog](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/390)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged after 31 days.

- **KTextEditor - [Implement multiple mouse selections with multicursor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/406)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix toggle comment with empty line in selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/407)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Vi input: fix find selected](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/405)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KSyntaxHighlighting - [Asciidoc: fix title highlighting when not starting at level 0 and section block continuation that should not be highlighted](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/345)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Bash/Zsh: add -o and -a logical operators with [ ... ]](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/343)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **KSyntaxHighlighting - [G-Code: fix number pattern, Mnnn as M-word or M-word (user), add String and...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/342)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **Kate - [Also update config page icons](https://invent.kde.org/utilities/kate/-/merge_requests/843)**<br />
Request authored by [Eric Armbruster](https://invent.kde.org/eric) and merged at creation day.

- **KSyntaxHighlighting - [Fortran: Interpret percent sign as operator](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/344)**<br />
Request authored by [Jakub Benda](https://invent.kde.org/albandil) and merged at creation day.

- **Kate - [Git: Show file history across renames](https://invent.kde.org/utilities/kate/-/merge_requests/840)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [[Keyboard Macros Plugin] Small GUI update fix after record cancelation](https://invent.kde.org/utilities/kate/-/merge_requests/841)**<br />
Request authored by [Pablo Rauzy](https://invent.kde.org/pablorackham) and merged after one day.

- **Kate - [[Split Views intuitive movements] add documentation for menu actions](https://invent.kde.org/utilities/kate/-/merge_requests/842)**<br />
Request authored by [Pablo Rauzy](https://invent.kde.org/pablorackham) and merged at creation day.

- **KTextEditor - [Dont enforce word boundary with multicursor occurunce select](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/403)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Update XMLGui clients names + fix show action for project toolview](https://invent.kde.org/utilities/kate/-/merge_requests/838)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [GitBranchDeletion: Allow selecting all branches quickly](https://invent.kde.org/utilities/kate/-/merge_requests/837)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Make statusbar configurable](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/402)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [RPM Spec: fix #20: support of ${expand:...}, ${lua:...}, lua section and some...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/340)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

#### Week 32

- **Kate - [Keyboard Macros Plugin](https://invent.kde.org/utilities/kate/-/merge_requests/823)**<br />
Request authored by [Pablo Rauzy](https://invent.kde.org/pablorackham) and merged after 15 days.

- **Kate - [Intuitive movements between split views](https://invent.kde.org/utilities/kate/-/merge_requests/836)**<br />
Request authored by [Pablo Rauzy](https://invent.kde.org/pablorackham) and merged after one day.

- **Kate - [allow for recolorable git icon](https://invent.kde.org/utilities/kate/-/merge_requests/833)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [New icons for toolviews](https://invent.kde.org/utilities/kate/-/merge_requests/831)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Fix git push dialog](https://invent.kde.org/utilities/kate/-/merge_requests/832)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [allow to configure printing font](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/401)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [VHDL: (#21) fix instantiating component in architecture](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/339)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [CartoCSS: fix the last statement in curly braces when it does not terminate with a semicolon](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/338)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [support of C++23 and C23](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/337)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [add documentation for the new &quot;Align On&quot; action, command, and JS function](https://invent.kde.org/utilities/kate/-/merge_requests/826)**<br />
Request authored by [Pablo Rauzy](https://invent.kde.org/pablorackham) and merged after 4 days.

- **KTextEditor - [New editing command proposal: align](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/391)**<br />
Request authored by [Pablo Rauzy](https://invent.kde.org/pablorackham) and merged after 14 days.

- **Kate - [Refactor QuickDialog](https://invent.kde.org/utilities/kate/-/merge_requests/830)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Search: Dont enable multiline search when regex mode is disabled](https://invent.kde.org/utilities/kate/-/merge_requests/820)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 15 days.

- **KTextEditor - [Multicursor: Allow skipping selected occurunce](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/398)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Update folding ranges on text modification](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/399)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Ignore folding ranges if document has changed](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/400)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [KateDocument: always add space after single line comment start marker](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/397)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **Kate - [Disable KRecentDocument:add](https://invent.kde.org/utilities/kate/-/merge_requests/828)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [add documentation for view.searchText](https://invent.kde.org/utilities/kate/-/merge_requests/825)**<br />
Request authored by [Pablo Rauzy](https://invent.kde.org/pablorackham) and merged after one day.

- **KTextEditor - [New commands for selecting text](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/394)**<br />
Request authored by [Pablo Rauzy](https://invent.kde.org/pablorackham) and merged after 8 days.

#### Week 31

- **Kate - [Sidebar: use a struct to store all ToolView info](https://invent.kde.org/utilities/kate/-/merge_requests/827)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **Kate - [(re) fix BUG: 412227](https://invent.kde.org/utilities/kate/-/merge_requests/824)**<br />
Request authored by [loh.tar](https://invent.kde.org/lohtar) and merged after 7 days.

- **KTextEditor - [Fix vi mode delete behavior](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/396)**<br />
Request authored by [Helga Albert-Huszár](https://invent.kde.org/helguli) and merged after 2 days.

- **KTextEditor - [Fix cursor position after calling an each-based command, when possible](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/392)**<br />
Request authored by [Pablo Rauzy](https://invent.kde.org/pablorackham) and merged after 10 days.

- **KSyntaxHighlighting - [Optimize AbstractHighlighter](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/336)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Resolve/compile regexp rules lazily](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/333)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Jam: add block comments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/335)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Zsh: add comment in [[ ... ]] and other fixes for [[ ... ]]](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/334)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [fix BUG: 457392](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/395)**<br />
Request authored by [Pablo Rauzy](https://invent.kde.org/pablorackham) and merged after one day.

- **KSyntaxHighlighting - [add CommonJS module and ECMAScript module extension for JS and TS](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/332)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Add support for multiple minted and lstlistings languages in LaTeX documents](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/309)**<br />
Request authored by [Rafał Lalik](https://invent.kde.org/fizyk) and merged after 100 days.

- **KSyntaxHighlighting - [add rockspec file to Lua](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/331)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Markdown: use zsh syntax for zsh code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/329)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Zsh: fix variable and string in a glob qualifier](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/330)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [reduce convertion to QString and use more QStringView](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/328)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Scheme: fix combined prefix number](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/325)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [add basic support of aspx and ashx](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/326)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Batch: fix many issues and add tests](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/324)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [R: add := operator](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/327)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

