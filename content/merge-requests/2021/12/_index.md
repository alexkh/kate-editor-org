---
title: Merge Requests - December 2021
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2021/12/
---

#### Week 52

- **Kate - [Show a less patronizing message when trying to run with sudo or kdesu](https://invent.kde.org/utilities/kate/-/merge_requests/541)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged at creation day.

- **Kate - [search: Allow filtering search results](https://invent.kde.org/utilities/kate/-/merge_requests/538)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [KParts::PartBase::setPluginLoadingMode() has been deprecated](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/242)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **KTextEditor - [Change build system to make building against qt6](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/235)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after 9 days.

- **Kate - [Fix blame not visible after switching to a &#x27;diff view&#x27;](https://invent.kde.org/utilities/kate/-/merge_requests/534)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 8 days.

- **Kate - [Various code fixes for building against Qt6](https://invent.kde.org/utilities/kate/-/merge_requests/540)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **KTextEditor - [Apply word filter on async completion models](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/241)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff) and merged after one day.

- **KSyntaxHighlighting - [systemd unit: update to systemd v250](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/290)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged at creation day.

- **KSyntaxHighlighting - [separate dynamic StringDetect rule to avoid copies in StringDetect::doMath()](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/289)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [LSP: don&#x27;t hardcode diagnostics colours](https://invent.kde.org/utilities/kate/-/merge_requests/539)**<br />
Request authored by [Jan Blackquill](https://invent.kde.org/cblack) and merged at creation day.

- **Kate - [Various small git improvements](https://invent.kde.org/utilities/kate/-/merge_requests/536)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [CMake plugin: Initial commit](https://invent.kde.org/utilities/kate/-/merge_requests/483)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 113 days.

- **Kate - [lsp: Do not HTML escape markdown text](https://invent.kde.org/utilities/kate/-/merge_requests/537)**<br />
Request authored by [Karthik Nishanth](https://invent.kde.org/nishanthkarthik) and merged at creation day.

- **KTextEditor - [Validates the input method attributes received from input method.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/239)**<br />
Request authored by [Xuetian Weng](https://invent.kde.org/xuetianweng) and merged at creation day.

- **KTextEditor - [The minimum Qt version has been 5.15 for a while now](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/240)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [some optimizations and rule transformations](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/288)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Utilize kcoreaddons_add_plugin CMake method](https://invent.kde.org/utilities/kate/-/merge_requests/404)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after 222 days.

#### Week 51

- **Kate - [git: Show last commit in branch delete dialog](https://invent.kde.org/utilities/kate/-/merge_requests/532)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **Kate - [GitBlame: Show error on failure](https://invent.kde.org/utilities/kate/-/merge_requests/535)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [Fix compile on Mac if EditorConfig is found](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/236)**<br />
Request authored by [Allen Winter](https://invent.kde.org/winterz) and merged at creation day.

- **Kate - [lsp: Handle client/registerCapability](https://invent.kde.org/utilities/kate/-/merge_requests/531)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Imporve current orgmode highlight support](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/287)**<br />
Request authored by [Gary Wang](https://invent.kde.org/garywang) and merged after one day.

- **Kate - [Fix incorrect blame info parsing when summary has &#x27;\t&#x27;](https://invent.kde.org/utilities/kate/-/merge_requests/533)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Change the build system to enable building with Qt6](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/286)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **Kate - [GDB: Use debug-run theme icon in stead of custom icon](https://invent.kde.org/utilities/kate/-/merge_requests/529)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **Kate - [Add basic Fossil support to Autoload Repositories](https://invent.kde.org/utilities/kate/-/merge_requests/530)**<br />
Request authored by [Javier Guerra](https://invent.kde.org/javier) and merged at creation day.

- **KSyntaxHighlighting - [SQL and SQL (PostgreSQL): nested comments support](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/285)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

#### Week 50

- **KSyntaxHighlighting - [GnuPlot: fix a lot of issue](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/284)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [PHP: add readonly, never and some functions/classes/constants of php-8.1](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/283)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Bash/Zsh: more unixcommands (GNU coreutils and some others) and fix ${!2}](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/282)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Fix compilation error on Qt6](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/280)**<br />
Request authored by [Yuhang Zhao](https://invent.kde.org/zhaoyuhang) and merged after one day.

- **KSyntaxHighlighting - [Fix language specification comments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/281)**<br />
Request authored by [Andreas Sturmlechner](https://invent.kde.org/asturmlechner) and merged after one day.

- **Kate - [lspclient: minor optimization and extend documentation](https://invent.kde.org/utilities/kate/-/merge_requests/528)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [lspclient: didSave notification](https://invent.kde.org/utilities/kate/-/merge_requests/527)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **KTextEditor - [Remove unused exporting of SwapFile class symbols](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/234)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **KSyntaxHighlighting - [Rename MIME type text/x-objcpp-src =&gt; text/x-objc++src](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/279)**<br />
Request authored by [Igor Kushnir](https://invent.kde.org/igorkushnir) and merged at creation day.

- **KSyntaxHighlighting - [New color theme Homunculus.theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/278)**<br />
Request authored by [shenleban tongying](https://invent.kde.org/slbtongying) and merged after 8 days.

#### Week 49

- **Kate - [Fix segfault on session change](https://invent.kde.org/utilities/kate/-/merge_requests/526)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [Fix cursor position after completion tail restore](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/233)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 3 days.

- **kate-editor.org - [Remove rawhtml shortcode](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/34)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged after one day.

- **Kate - [Fix git blame for files with DOS end of line](https://invent.kde.org/utilities/kate/-/merge_requests/525)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **KTextEditor - [Introduce PCH support](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/227)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 25 days.

- **KTextEditor - [Color current indentation line differently](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/228)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 24 days.

- **KSyntaxHighlighting - [Add grammar for RETRO Forth](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/268)**<br />
Request authored by [Jan Blackquill](https://invent.kde.org/cblack) and merged after 47 days.

- **Kate - [enable/disable project actions](https://invent.kde.org/utilities/kate/-/merge_requests/523)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 4 days.

- **Kate - [Fix LSPTooltip sizing](https://invent.kde.org/utilities/kate/-/merge_requests/524)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Vimode-keyparser: Make some functions more efficient](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/232)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Use KTextEditor::Range by value](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/231)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 48

- **Kate - [SQL Plugin &quot;select data&quot; action for tables](https://invent.kde.org/utilities/kate/-/merge_requests/521)**<br />
Request authored by [Artyom Kirnev](https://invent.kde.org/arti) and merged after 7 days.

- **Kate - [Bug: 411920 Fix - Implementation of SessionConfigInterface for SQL plugin](https://invent.kde.org/utilities/kate/-/merge_requests/522)**<br />
Request authored by [Artyom Kirnev](https://invent.kde.org/arti) and merged after 3 days.

- **KSyntaxHighlighting - [markdown.xml: Fix highlighting of headers (include last char)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/277)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 5 days.

