---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Pridobi Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux in unixi

+ Namesti [Kate](https://apps.kde.org/en/kate) ali [KWrite](https://apps.kde.org/en/kwrite) iz [vaše distribucije](https://kde.org/distributions/)

{{< appstream_badges >}}

+ [Kate's Snap package v Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Izdaja Kate: (64-bitni) paket AppImage](https://binary-factory.kde.org/job/Kate_Release_appimage/) *
+ [Nočna gradnja Kate: (64-bitni) paket AppImage](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **
+ [Build it](/build-it/#linux) iz vira.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate v trgovini Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate prek Chocolatey](https://chocolatey.org/packages/kate)
+ [Namestilnik izdaje Kate (64-bitno)](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Namestilnik nočne gradnje Kate (64-bitno)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Izgradite ga](/build-it/#windows) iz izvorne kode.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Namestilnik izdaje Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Namestilnik nočne gradnje Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Izgradite ga](/build-it/#mac) iz izvorne kode.

{{< /get-it >}}

{{< get-it-info >}}

**O izdajah:** <br> [Kate](https://apps.kde.org/en/kate) in [KWrite](https://apps.kde.org/en/kwrite) sta dela [KDE Applications](https://apps.kde.org), ki so [praviloma masovno izdane 3-krat na leto](https://community.kde.org/Schedules). [Urejevalnik besedil](https://api.kde.org/frameworks/ktexteditor/html/) in [osvetljevanje skladnje](https://api.kde.org/frameworks/syntax-highlighting/html/) zagotavlja [KDE Frameworks] v [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), ki so [mesečno posodobljen](https://community.kde.org/Schedules/Frameworks). Nove izdaje so objavljene [tukaj](https://kde.org/announcements/).

\*  Paketi **ob izdaji** vsebujejo zadnjo verzijo programa Kate in KDE Frameworks.

\*\* The **nightly** packages are automatically compiled daily from source code, therefore they may be unstable and contain bugs or incomplete features. These are only recommended for testing purposes.

{{< /get-it-info >}}
