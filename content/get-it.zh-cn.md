---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: 获取 Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux 与 Unix

+ 从 [您的发行版](https://kde.org/distributions/) 安装 [Kate](https://apps.kde.org/en/kate) 或 [KWrite] (https://apps.kde.org/en/kwrite)

{{< appstream_badges >}}

+ [Kate 在 Snapcraft 上的 Snap 软件包](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate 正式版 (64bit) AppImage 软件包](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Kate 每夜构建版 (64bit) AppImage 软件包](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ 从源码[构建](/build-it/#linux)。

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate 的 Microsoft Store 页面](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate 的 Chocolatey 页面](https://chocolatey.org/packages/kate)
+ [Kate 正式版 (64 位) 安装程序](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Kate 每夜测试版 (64位) 安装程序](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ 从源码[构建](/build-it/#windows)。

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Kate 正式版安装程序](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Kate 每夜测试版安装程序](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ 从源码[构建](/build-it/#mac)。

{{< /get-it >}}

{{< get-it-info >}}

**关于发布：** <br> [Kate](https://apps.kde.org/en/kate) 与 [KWrite](https://apps.kde.org/en/kwrite) 是 [KDE Applications](https://apps.kde.org) 的一部分，通常 [每年集体发布三次](https://community.kde.org/Schedules)。[文本编辑](https://api.kde.org/frameworks/ktexteditor/html/) 和 [语法高亮](https://api.kde.org/frameworks/syntax-highlighting/html/) 引擎由 [KDE 框架](https://kde.org/announcements/kde-frameworks-5.0/) 提供，[每月更新](https://community.kde.org/Schedules/Frameworks)。新发布将公布 [于此](https://kde.org/announcements/)。

\* **release** 软件包包含最新版本的 Kate 和 KDE 框架。

\*\* **nightly** 软件包每日自动从源代码编译，因此可能不是很稳定，并且包含缺陷或不完整的功能。仅推荐用于测试用途。

{{< /get-it-info >}}
