---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Pobierz Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux i Uniksy

+ Wgraj [Kate](https://apps.kde.org/en/kate) lub [KWrite](https://apps.kde.org/en/kwrite) ze [swojej dystrybucji](https://kde.org/distributions/)

{{< appstream_badges >}}

+ [Pakiet Snap Kate w Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Stabilne Kate (64bit) jako AppImage](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Rozwojowe Kate (64bit) jako AppImage](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Zbuduj to](/build-it/#linux) ze źródła.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate w Sklepie Microsoft](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate przez Chocolatey](https://chocolatey.org/packages/kate)
+ [Instalator wersji Kate (64bit)](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Rozwojowa Kate (64bit) jako instalator](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Zbuduj to](/build-it/#windows) ze źródła.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Stabilna Kate jako instalator](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Rozwojowa Kate jako instalator](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Zbuduj to](/build-it/#mac) ze źródła.

{{< /get-it >}}

{{< get-it-info >}}

**O wydaniach:** <br> [Kate](https://apps.kde.org/en/kate) oraz[KWrite](https://apps.kde.org/en/kwrite) są częścią [Aplikacji KDE](https://apps.kde.org), które są [wydawane zazwyczaj 3 razy do roku wszystkie razem](https://community.kde.org/Schedules). Silniki [edytowania tekstu](https://api.kde.org/frameworks/ktexteditor/html/) oraz [podświetlania składni](https://api.kde.org/frameworks/syntax-highlighting/html/) są dostarczane przez[Szkielety KDE](https://kde.org/announcements/kde-frameworks-5.0/), które są [uaktualniane co miesiąc](https://community.kde.org/Schedules/Frameworks). Nowe wydania są ogłaszane [tutaj](https://kde.org/announcements/).

\* **Stabilne** pakiety zawierają najświeższe wersje Kate oraz Szkieletów KDE.

\*\* **Rozwojowe** pakiety są codziennie kompilują się same z kodu źródłowego, stąd mogą się okazać niestabilne i zawierać błędy lub niedokończone funkcje. Są zalecane tylko do prób.

{{< /get-it-info >}}
