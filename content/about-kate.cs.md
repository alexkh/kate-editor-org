---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: Vlastnosti
---
## Vlastnosti aplikace

![Screenshot showing Kate window splitting feature and search & replace plugin](/images/kate-window.webp)

+ View and edit multiple documents at the same time, by splitting horizontally and vertically the window
+ Lot of plugins: [Embedded terminal](https://konsole.kde.org), SQL plugin, build plugin, GDB plugin, Replace in Files, and more
+ Multi-document interface (MDI)
+ Podpora sezení

## Obecné vlastnosti

![Screenshot showing Kate search and replace feature](/images/kate-search-replace.png)

+ Encoding support (Unicode and lots of others)
+ Podpora obousměrného vykreslování textu
+ Line ending support (Windows, Unix, Mac), including auto detection
+ Network transparency (open remote files)
+ Extensible through scripting

## Advanced Editor Features

![Screenshot of Kate border with line number and bookmark](/images/kate-border.png)

+ Bookmarking system (also supported: break points etc.)
+ Scroll bar marks
+ Line modification indicators
+ Čísla řádek
+ Skládání kódu

## Zvýrazňování syntaxe

![Screenshot of Kate syntax highlighting features](/images/kate-syntax.png)

+ Highlighting support for over 300 languages
+ Bracket matching
+ Smart on-the-fly spell checking
+ Highlighting of selected words

## Programming Features

![Screenshot of Kate programming features](/images/kate-programming.png)

+ Scriptable auto indentation
+ Smart comment and uncomment handling
+ Automatické doplňování s nápovědou argumentů
+ Vstupní režim VI
+ Rectangular block selection mode

## Najít a nahradit

![Screenshot of Kate incremental search feature](/images/kate-search.png)

+ Incremental search, also known as &#8220;find as you type&#8221;
+ Support for multiline search & replace
+ Podpora pro regulární výrazy
+ Search & replace in multiple opened files or files on disk

## Záloha a obnovení

![Screenshot of Kate recover from crash feature](/images/kate-crash.png)

+ Vytvořit zálohy při uložení
+  Zálohujte si soubory a systémové oblasti
+ Undo / redo system
