---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Obtenir Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unix

+ Installer [Kate](https://apps.kde.org/en/kate) ou [KWrite](https://apps.kde.org/en/kwrite) à partir de [votre distribution](https://kde.org/distributions/)

{{< appstream_badges >}}

+ [Paquet « Snap » pour Kate dans « Snapcraft » ](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Paquet « AppImage » (64 bits) pour la mise à jour de Kate](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Paquet « AppImage » (64 bits) pour la version de développement de Kate](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Compiler Kate](/build-it/#linux) à partir des sources.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate dans la boutique « Windows »](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate par « Chocolatey »](https://chocolatey.org/packages/kate)
+ [Installateur de la version Kate (64 bits)](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Installateur de Kate en version de développement (64 bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Compiler Kate](/build-it/#windows) à partir des sources.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### MacOS

+ [Installateur de la version Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Installateur de la version de développement de Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Compiler Kate](/build-it/#mac) à partir des sources.

{{< /get-it >}}

{{< get-it-info >}}

**A propos des versions :** <br>[Kate](https://apps.kde.org/en/kate) et [KWrite](https://apps.kde.org/en/kwrite) font partie des [applications KDE](https://apps.kde.org), qui sont [publiées en général trois fois par an de façon complète](https://community.kde.org/Schedules). Le [moteur de modification de texte](/about-katepart/) et la [coloration syntaxique](/syntax/) sont fournis par [les environnements de développement de KDE](https://kde.org/announcements/kde-frameworks-5.0/), [mis à jour mensuellement](https://community.kde.org/Schedules/Frameworks). De nouvelles versions sont annoncées [ici](https://kde.org/announcements/).

\* Les paquets de **version** contiennent la dernière version de Kate et des environnements de développement de KDE.

\*\* Les paquets de **développement** sont automatiquement compilés quotidiennement à partir du code source. Cependant, ils pourraient être instables et contenir des bogues ou des fonctionnalités incomplètes. Ils ne sont recommandés que pour des besoins de tests.

{{< /get-it-info >}}
