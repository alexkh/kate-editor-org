---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Obtener Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux y Unix

+ Instalar [Kate](https://apps.kde.org/es/kate) o [KWrite](https://apps.kde.org/es/kwrite) en [su distribución](https://kde.org/distributions/)

{{< appstream_badges >}}

+ [Paquete Snap de Kate en Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Paquete AppImage del último lanzamiento de Kate (64 bits)](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Paquete AppImage de la compilación diaria de Kate (64 bits)](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Compilar](/build-it/#linux) el código fuente.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate en la Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate mediante Chocolatey](https://chocolatey.org/packages/kate)
+ [Instalador de la versión de Kate (64 bits)](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Instalador de la compilación diaria de Kate (64 bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Compilar](/build-it/#windows) el código fuente.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Instalador del último lanzamiento de Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Instalador de la compilación diaria de Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Compilar](/build-it/#mac) el código fuente.

{{< /get-it >}}

{{< get-it-info >}}

**Acerca de los lanzamientos:** <br>[Kate](https://apps.kde.org/es/kate) y [KWrite](https://apps.kde.org/es/kwrite) forman parte de las [Aplicaciones de KDE](https://apps.kde.org), que [se publican en conjunto unas 3 veces al año](https://community.kde.org/Schedules). Los motores de [edición de texto](https://api.kde.org/frameworks/ktexteditor/html/) y de [resaltado de sintaxis](https://api.kde.org/frameworks/syntax-highlighting/html/) los proporciona [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), que [se actualiza cada mes](https://community.kde.org/Schedules/Frameworks). Las nuevas versiones se anuncian [aquí](https://kde.org/announcements/).

\* Los paquetes del **último lanzamiento** contienen la última versión de Kate y de KDE Frameworks.

\*\* Los paquetes de la **compilación diaria** se compilan automáticamente cada día a partir del código fuente, por lo que pueden ser inestables y contener errores o funcionalidades incompletas. Solo se recomiendan para hacer pruebas.

{{< /get-it-info >}}
