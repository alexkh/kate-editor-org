---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: Característiques
---
## Característiques de l'aplicació

![Captura de pantalla que mostra la característica de finestra dividida i del connector de busca i substitució de Kate](/images/kate-window.webp)

+ Vista i edició de múltiples documents a la vegada, dividint la finestra horitzontalment i verticalment
+ Molts connectors: [Terminal incrustat](https://konsole.kde.org), connector SQL, connector de construcció, connector de GDB, Substitució a fitxers, i més
+ Interfície multidocument (MDI)
+ Suport de sessions

## Característiques generals

![Captura de pantalla que mostra la característica de busca i substitució](/images/kate-search-replace.png)

+ Implementació de la codificació (Unicode i molts altres)
+ Implementació de la Representació de text bidireccional
+ Implementació del final de línia (Windows, Unix, Mac), incloent-hi la detecció automàtica
+ Transparència de xarxa (obri fitxers remots)
+ Ampliable a través de la creació de scripts

## Característiques avançades d'edició

![Captura de pantalla de la vora de Kate amb el número de línia i un punt](/images/kate-border.png)

+ Sistema de punts (també s'admeten: punts de ruptura, etc.)
+ Marques de barra de desplaçament
+ Indicadors de modificació de línia
+ Números de línies
+ Plegat de codi

## Ressaltat de sintaxi

![Captura de pantalla de les característiques de ressaltat de sintaxi de Kate](/images/kate-syntax.png)

+ El ressaltat de sintaxi admet més de 300 llenguatges
+ Concordança de parèntesis
+ Verificació ortogràfica al vol intel·ligent
+ Ressaltat de paraules seleccionades

## Característiques de programació

![Captura de pantalla de les característiques de programació](/images/kate-programming.png)

+ Sagnat automàtic amb creació de scripts
+ Gestió intel·ligent de comentaris i descomentaris
+ Compleció automàtica amb consells d'arguments
+ Mode d'entrada de Vi
+ Mode de selecció de bloc rectangular

## Busca i substitució

![Captura de pantalla de la característica de busca incremental de Kate](/images/kate-search.png)

+ Busca incremental, també coneguda com a «busca en teclejar»
+ Permet la busca i substitució multilínia
+ Admet expressions regulars
+ Busca i substitució en fitxers múltiples oberts o fitxers del disc

## Còpia de seguretat i restauració

![Captura de pantalla de la característica de recuperació d'una fallada de Kate](/images/kate-crash.png)

+ Còpia de seguretat en guardar
+ Fitxers d'intercanvi per a recuperar les dades en cas de fallada del sistema
+ Sistema de desfer/refer
