---
title: Build It
hideMeta: true
author: Christoph Cullmann
date: 2016-06-13T15:15:02+00:00
menu:
  main:
    weight: 4
---

# Building Kate from Sources on Linux {#linux}

Right now, Kate's source code is located on the [invent.kde.org](https://invent.kde.org/utilities/kate) GitLab instance.
This tutorial explains how to build Kate without touching your global installation.

All stuff will happen inside an extra development directory.
This avoids the need for administrator rights and allows always to fallback to use the global installed Kate if the currently build version is broken.

As this tutorial will introduce a setup that allows to develop other parts of the KDE software stack, too, we will talk in the following about setting up a KDE development environment.
Required KDE Frameworks will be compiled in addition to the Kate application parts.
This eases contributions to all levels of the KDE software stack.

## Choose your Kate & KDE development directory

Choose yourself some path where all Kate & KDE development things should end up.
Beside user local configuration files, nothing outside of this directory will be polluted.
You can re-use this for development on any part of the KDE software stack.

For the remaining parts of this description we use

{{< highlight bash >}}
~/projects/kde
{{< / highlight >}}

Feel free to customize this.

## Install kdesrc-build

Installing is more or less just cloning the current version

{{< highlight bash >}}
mkdir -p ~/projects/kde/src
cd ~/projects/kde/src
git clone https://invent.kde.org/sdk/kdesrc-build.git
{{< / highlight >}}

For later ease of use, best symlink the kdesrc-build script to some folder inside your path, e.g. if you have some user local bin:

{{< highlight bash >}}
ln -s ~/projects/kde/src/kdesrc-build/kdesrc-build ~/bin
{{< / highlight >}}

## Configuring kdesrc-build

kdesrc-build has some setup pass that can configure stuff for you, see [this introduction](https://community.kde.org/Get_Involved/development#Set_up_kdesrc-build).

Here we just show a **.config/kdesrc-buildrc** that is good enough for Kate development needs.

You can just copy and paste the below variant into your home directory as **.config/kdesrc-buildrc** and adapt the paths to your needs.
If you stick with the **~/projects/kde** path we did choose above, this should be usable 1:1.

{{< highlight bash >}}
global
    # use the latest KF5 and Qt5-based software.
    branch-group kf5-qt5

    # we want .kateproject files with ninja
    cmake-options -G "Kate - Ninja"

    # clangd tooling
    compile-commands-export yes
    compile-commands-linking yes

    # Install directory for KDE software
    kdedir ~/projects/kde/usr

    # Directory for downloaded source code
    source-dir ~/projects/kde/src

    # Directory to build KDE into before installing
    # relative to source-dir by default
    build-dir ~/projects/kde/build
end global

include ~/projects/kde/src/kdesrc-build/kf5-frameworks-build-include
include ~/projects/kde/src/kdesrc-build/kf5-applications-build-include
include ~/projects/kde/src/kdesrc-build/kf5-workspace-build-include
include ~/projects/kde/src/kdesrc-build/kf5-extragear-build-include
{{< / highlight >}}

The important lines to have a good experience developing Kate (or other KDE stuff) with Kate are described below in detail.

### Create .kateproject files

{{< highlight bash >}}
    # we want .kateproject files with ninja
    cmake-options -G "Kate - Ninja"
{{< / highlight >}}

This ensures you get not only Ninja build files (nifty as this will e.g. automatically use multiple cores for compiles) but in addition .kateproject files inside the build directories for later use, too.
With this files, the project plugin of Kate will know what to do, e.g. where the source directory for the compile is and which build commands it should use for the build plugin.

### Ensure LSP integration works

{{< highlight bash >}}
    # clangd tooling
    compile-commands-export yes
    compile-commands-linking yes
{{< / highlight >}}

This ensures CMake will generate the **compile_commands.json** files that are required for clangd based LSP integration.
Beside just generating them inside the build directory the linking option will symlink them back to your source directories.
This allows zero-configuration usage of the LSP plugin inside Kate (and other editors).

## Install needed dependencies (optional)

**kdesrc-build** provides some initial setup mode to install the needed packages for several common distributions to start developing.
This is helpful if you are not sure if all needed development packages are installed on your system and you don't want to take care of this yourself manually.
**As this will try to install packages for your distribution, this step will require administrator rights for the package installation.**
To do that, just trigger:

{{< highlight bash >}}
kdesrc-build --initial-setup
{{< / highlight >}}

We already created a **.config/kdesrc-buildrc**, that will be left untouched.
For the further usage you don't need to modify your shell settings either.

## Build Kate & dependencies

To trigger a compile of Kate and all needed KDE dependencies now just type:

{{< highlight bash >}}
kdesrc-build --include-dependencies kate
{{< / highlight >}}

For me, this did now compile 76 modules ;)
That takes a few minutes on a recent machine.
Time to grab some coffee or think about what you actually want to implement.

If you only want to build Kate without the dependencies because you are sure you have recent enough stuff on your system, you can try:

{{< highlight bash >}}
kdesrc-build --no-include-dependencies kate
{{< / highlight >}}

But given that on modern machines, the compile times are low, it is more convenient to just build all stuff, that ensures you have e.g. the latest and best KSyntaxHighlighting and KTextEditor frameworks around, too!

Given this setup, you are free to build all KDE software parts with a simple:

{{< highlight bash >}}
kdesrc-build
{{< / highlight >}}

Keep in mind: that will take some time (and disk space).

## How to use the build stuff?

To launch you local Kate version, you need to setup the environment first properly to ensure the right plugins and stuff is loaded.
Fortunately this is very simple:

{{< highlight bash >}}
source ~/projects/kde/build/kde/applications/kate/prefix.sh
kate
{{< / highlight >}}

A nifty way to wrap this is to e.g. create yourself a small wrapper script to start your master branch version of Kate inside your local bin directory:

{{< highlight bash >}}
#!/bin/bash
source ~/projects/kde/build/kde/applications/kate/prefix.sh
exec kate "$@"
{{< / highlight >}}

## Keep your stuff up-to-date

To keep your local version up-to-date, you can just use the above commands again.
They will take care of pulling new changes from the KDE repositories and building/installing them into your local prefix.

## Develop!

Now, the remaining question is: How to develop best?

Naturally, if you want to hack on Kate, it might make sense to use Kate for that.

Given the above preparations, that is easy to do, just start your new master version of Kate and pass it the build directory:

{{< highlight bash >}}
kate ~/projects/kde/build/kde/applications/kate
{{< / highlight >}}

Alternatively, you can navigate there in your terminal and startup Kate from there, it will auto-open it:

{{< highlight bash >}}
cd ~/projects/kde/build/kde/applications/kate
kate
{{< / highlight >}}

To have the best experience with this, ensure you have at least project & LSP plugin enabled.
If you like to have some GUI build integration, activate the build plugin, too.

You will end up with a new Kate windows like shown below.

<p align="center">
    <a href="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-startup.png" target="_blank"><img width=700 src="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-startup.png"/></a>
</p>

In the lower "Current Project" tool view you have per default two terminals.
The first terminal is inside your build directory, here you can e.g. run your **ninja** and **ninja install** commands and such.
The second terminal is inside your source directory, perfect for e.g. git command line calls.

Given the above setup, the LSP plugin (if you have **clangd** installed) should work out of the box.

<p align="center">
    <a href="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-lsp.png" target="_blank"><img width=700 src="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-lsp.png"/></a>
</p>

Other nifty stuff like project wide quick open, search & replace and correct build targets should be setup, too.

<p align="center">
    <a href="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-search.png" target="_blank"><img width=700 src="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-search.png"/></a>
</p>

<p align="center">
    <a href="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-build.png" target="_blank"><img width=700 src="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-build.png"/></a>
</p>


## Contribute back!

Feel free to create a merge request at [invent.kde.org](https://invent.kde.org/utilities/kate/merge_requests).
For more information see [Join Us](/join-us/).

## Get Support?

If you have questions you can ask them on our mailing list [kwrite-devel@kde.org](mailto:kwrite-devel@kde.org) and/or join #kate on irc.libera.chat.

For more information see [Get Help](/support/).

# Building Kate from Sources on Windows {#mac}

To build the KF5 based Kate/KWrite you can follow the guide on [KDE on Windows](https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Windows).

# Building Kate from Sources on macOS {#mac}

To build the KF5 based Kate/KWrite you can follow the guide on [KDE on macOS](https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Mac).
