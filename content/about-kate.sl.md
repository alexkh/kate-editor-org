---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: Zmožnosti
---
## Zmožnosti aplikacije

![Posnetek zaslona prikazuje možnost razdeljenega okna in vtičnika iskanja in zamenjave](/images/kate-window.webp)

+ Oglejte si in uredite več dokumentov hkrati, tako da okno razdelite vodoravno in navpično
+ Veliko vtičnikov: [vdelani terminal] (https://konsole.kde.org), vtičnik SQL, vtičnik za izgradnjo, vtičnik GDB, Zamenjava v datoteka in drugo
+ Več dokumentni vmesnik (MDI)
+ Podpora seje

## Splošne zmožnosti

![Posnetek zaslona prikazuje iskanje in zamenjavo nizov znakov](/images/kate-search-replace.png)

+ Podpora kodnim tabelam (Unicode in veliko drugih)
+ Podpora obojesmernim pisanjem besedil
+ Podpora končnicam vrstic ((Windows, Unix, Mac), vključno s samodetekcijo
+ Transparentnost v omrežju (odpiranje oddaljenih datotek)
+ Razširljiv s skripti

## Napredne zmožnosti urejanja

![Posnetek zaslona z robom s številkami vrstic in zaznamkom](/images/kate-border.png)

+ Sistem zaznamkov (podprte tudi: prelomne točke itd.)
+ Oznake na drsniku
+ Indikatorji spremenjenih vrstic
+ Številke vrstic
+ Pregibanje kode

## Osvetljevanje skladnje

![Posnetek zaslona z osvetljevanjem skladnje](/images/kate-syntax.png)

+ Podpora za osvetljevanje za več kot 300 jezikov
+ Ujemanje oklepajev
+ Pametno sprotno preverjanje črkovanja
+ Osvetljevanje izbranih besed

## Zmožnosti programiranja

![Posnetek zaslona prikazuje zmožnosti programiranja Kate](/images/kate-programming.png)

+ Skriptno določeno samodejno zamikanje
+ Pametni komentarji in odstranjevanje komentarjev
+ Samodejno zaključevanje z namigi glede parametrov
+ Vhodni način VI
+ Način izbire v pravokotnikih

## Iskanje & zamenjava

![Posnetek zaslona Kate s postopnim iskanjem niza besedila](/images/kate-search.png)

+ Inkrementalno iskanje znano tudi kot &#8220;išči ko tipkaš&#8221;
+ Podpora za večvrstično iskanje in zamenjavo
+ Podpora regularnim izrazom
+ Poišče in zamenja več odprtih datotek ali datotek na disku

## Izdelava varnostne kopije in obnova iz nje

![Posnetek zaslona obnove Kate po izpadu računalnika](/images/kate-crash.png)

+ Izdelava varnostne kopije ob shranjevanju
+ Zamenjajte datoteke za obnovitev podatkov ob zrušitvi sistema
+ Razveljavi / uveljavi sistem
