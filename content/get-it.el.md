---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Αποκτήστε το Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unices

+ Εγκατάσταση του [Kate](http://apps.kde.org/en/kate) ή του [KWrite](https://apps.kde.org/en/kwrite) από [τη διανομή σας](ηττπσ/κδε.οργ/distributions/)

{{< appstream_badges >}}

+ [Το πακέτο Snap του Kate στο Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate's release (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Kate's nightly (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Κατασκευάστε το](/build-it/#linux) από τον πηγαίο κώδικα.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Το Kate στο Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Το Kate μέσω Chocolatey](https://chocolatey.org/packages/kate)
+ [Kate release (64bit) installer](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Πρόγραμμα εγκατάσταης Kate nightly (64bit)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Κατασκευάστε το](/build-it/#windows) από τον πηγαίο κώδικα.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Πρόγραμμα εγκατάστασης της κυκλοφορίας του Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Πρόγραμμα εγκατάστασης του Kate nightly](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Κατασκευάστε το](/build-it/#mac) από τον πηγαίο κώδικα.

{{< /get-it >}}

{{< get-it-info >}}

**Σχετικά με τις εκδόσεις:** <br> Τα [Kate](https://apps.kde.org/en/kate) και [KWrite](https://apps.kde.org/en/kwrite) ανήκουν στις [KDE Εφαρμογές](https://apps.kde.org), οι οποίες [εκδίδονται τυπικά όλες μαζί 3 φορές το χρόνο](https://community.kde.org/Schedules). Οι μηχανές [επεξεργασίας κειμένου](https://api.kde.org/frameworks/ktexteditor/html/) και [τονισμού σύνταξης](https://api.kde.org/frameworks/syntax-highlighting/html/) παρέχονται από το [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), το οποίο [ενημερώνεται μηνιαίως](https://community.kde.org/Schedules/Frameworks). Οι νέες εκδόσεις ανακοινώνονται [εδώ](https://kde.org/announcements/).

\* Τα πακέτα **release** (κυκλοφορίας) περιέχουν την τελευταία έκδοση του Kate και του KDE Frameworks.

\*\* Τα πακέτα **nightly** μεταγλωττίζονται αυτόματα σε καθημερινή βάση από τον πηγαίο κώδικα και πιθανόν να είναι ασταθή και να περιέχουν σφάλματα ή ελλιπή χαρακτηριστικά. Συνιστώνται μόνο για δοκιμαστική χρήση.

{{< /get-it-info >}}
