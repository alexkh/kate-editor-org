---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Kate herunterladen
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unix-Varianten

+ Installieren Sie [Kate](https://apps.kde.org/en/kate) oder [KWrite](https://apps.kde.org/en/kwrite) von [Ihrer Distribution](https://kde.org/distributions/)

{{< appstream_badges >}}

+ [Snap-Paket von Kate in Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate's release (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Kate's nightly (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Aus dem Quelltext](/build-it/#linux) übersetzen.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate im Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate über Chocolatey](https://chocolatey.org/packages/kate)
+ [Kate release (64bit) installer](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Kate -Installationsprogramm (64bit) - Entwickler-Version](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Aus dem Quelltext](/build-it/#windows) übersetzen.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Kate-Installationsprogramm - Standard-Version](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Kate-Installationsprogramm - Entwickler-Version](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) **
+ [Aus dem Quelltext](/build-it/#mac) übersetzen.

{{< /get-it >}}

{{< get-it-info >}}

**Über die Veröffentlichungen:** <br> [Kate](https://apps.kde.org/en/kate) und [KWrite](https://apps.kde.org/en/kwrite) sind Teil der [KDE-Anwendungen](https://apps.kde.org), die normalerweise [3-mal pro Jahr gemeinsam](https://community.kde.org/Schedules) veröffentlicht werden. Der [Texteditor](https://api.kde.org/frameworks/ktexteditor/html/) und die Bibliothek [Syntaxhervorhebung](https://api.kde.org/frameworks/syntax-highlighting/html/) werden von [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/0) bereitgestellt, die  [monatlich aktualisiert](https://community.kde.org/Schedules/Frameworks) wird. Neue Versionen werden [hier](https://kde.org/announcements/) angekündigt.

\* Die **Standard**-Pakete enthalten die letzte Version von Kate und KDE Frameworks.

\*\* Die **Entwickler**-Pakete werden täglich automatisch aus dem Quellcode kompiliert kompiliert, daher können sie instabil sein und Fehler oder unvollständige Funktionen enthalten. Diese werden nur zu Testzwecken empfohlen.

{{< /get-it-info >}}
