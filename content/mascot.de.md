---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: Kate-Maskottchen
---
Kate's mascot, Kate the Cyber Woodpecker, was designed by [Tyson Tan](https://www.tysontan.com/).

The current evolution of our mascot was first unveiled [in April 2021](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## „Kate the Cyber Woodpecker“

![Kate the Cyber Woodpecker](/images/mascot/electrichearts_20210103_kate_normal.png)

## Version ohne Fußtext

![Kate the Cyber Woodpecker - Without Text](/images/mascot/electrichearts_20210103_kate_notext.png)

## Version ohne Hintergrund

![Kate the Cyber Woodpecker - Ohne Hintergrund](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Lizenzierung

For the licensing, to quote Tyson:

> I hereby donate the new Kate the Cyber Woodpecker to Kate Editor project. The artworks are dual licensed under LGPL (or whatever the same as Kate Editor) and Creative Commons BY-SA. In this way you don't need to ask me for permission before using/modifying the mascot.

## Kate the Cyber Woodpecker's design history

I asked Tyson if I can share the intermediate versions we exchanged to show the evolution and he agreed and even wrote some short summaries about the individual steps. This includes the initial mascot and the icon that fit into this design evolution. I therefore just quote what he wrote about the different steps below.

### 2014 - Kate the Woodpecker

![2014 version - Kate the Woodpecker](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> The 2014 version was done perhaps during my lowest point as an artist. I had not been drawing much in years, and was on the verge of giving up completely. There were some interesting ideas in this version, namely the {} brackets on her chest, and the color scheme inspired by doxygen code highlighting. However, the execution was very simplistic -- at that time, I deliberately stop using any fancy techniques to expose my inadequate base skills. In the followed years, I would be going through an arduous process to rebuild my broken, self-taught art foundation from the ground up. I'm grateful that the Kate team still kept this one on their website for many years.

### 2020 - Das neue Kate-Symbol

![2020 - Das neue Kate-Symbol](/images/mascot/history/kate-3000px.png)

> The new Kate icon was the only time I attempted real vector art. It was at that time I began to develop some faint artistic sense. I had no previous experience of icon design, there was zero discipline like math-guided proportion. Every shape and curve was tweaked using my instinct as an artist. The concept was very simple: it's a bird shaped like the letter "K", and it has to be sharp. The bird was chunkier until Christoph pointed it out, I later turned it into a more sleek shape.

### 2021 - Mascot sketch

![2021 version - Sketch of the new mascot](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> In this initial sketch, I was tempted to ditch the robotic concept and just draw a cute good looking bird instead.

### 2021 - Mascot design

![2021 version - Redesign of the new mascot](/images/mascot/history/electrichearts_20210103_kate_design.png)

> Kate's mascot re-design resumed after I finished drawing the new splash picture for the upcoming Krita 5.0. I was able to push myself to do more each time I attempted a new picture at that time, so I was determined to keep the robotic concept. Although the proportion was off, the mech design elements was more or less coined in this one.

### 2021 - Final Mascot

![2021 version - Final Kate the Cyber Woodpecker](/images/mascot/electrichearts_20210103_kate_normal.png)

> The proportion was adjusted after Christoph pointed it out. The pose was adjusted to feel more comfortable. The wings were re-drawn after studying on real bird wings. A cool-looking, detailed keyboard was hand-drawn to emphasize the main usage of the application. An abstract background was added with a proper 2D typography design. The general idea is: unmistakably cutting edge and artificial, but also unmistakably full of life and humanity.
