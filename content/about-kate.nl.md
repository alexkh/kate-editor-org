---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: Mogelijkheden
---
## Mogelijkheden van toepassing

![Schermafdruk die venster van Kate toont met die functie zoeken & vervangen splitst](/images/kate-window.webp)

+ Bekijk en bewerk meerdere documenten tegelijk, door het venster horizontaal en verticaal te splitsen
+ Veel plug-ins: [Ingebedde terminal](https://konsole.kde.org), SQL-plug-in, bouwplug-in, GDB-plug-in, vervangen in bestanden en meer
+ Interface voor meervoudig document (MDI)
+ Ondersteuning voor sessie

## Algemene mogelijkheden

![Schermafdruk die de functie zoeken en vervangen in Kate toont](/images/kate-search-replace.png)

+ Ondersteuning voor codering (Unicode en vele anderen)
+ Ondersteuning voor tonen van bi-directionele tekst
+ Ondersteuning voor regeleinde (Windows, Unix, Mac), inclusief autodetectie
+ Netwerktransparantie (open bestanden op afstand)
+ Uit te breiden via scripts

## Geavanceerde bewerkingsfuncties

![Schermafdruk van rand van Kate met regelnummer en bladwijzer](/images/kate-border.png)

+ Systeem voor bladwijzers (ook ondersteund: breakpoints etc.)
+ Markeringen op schuiflbalk
+ Markeringen voor wijzigingen in regels
+ Regelnummers
+ Code invouwen

## Syntaxisaccentuering

![Schermafdruk van functies voor syntaxisaccentuering van Kate](/images/kate-syntax.png)

+ Ondersteuning voor accentuering voor meer dan 300 talen
+ Haken overeen laten komen
+ Intelligente on-the-fly spellingcontrole
+ Accentuering van geselecteerde woorden

## Programmeringsmogelijkheden

![Schermafdruk van functies voor programmering van Kate](/images/kate-programming.png)

+ Automatisch inspringen mogelijk met script
+ Slimme toelichting en behandeling deze verwijderen
+ Automatische aanvulling met argumenten tips
+ VI-invoermethode
+ Rechthoekige selectiemodus van blokken

## Zoeken en vervangen

![Schermafdruk met de incrementele zoekfunctie van Kate](/images/kate-search.png)

+ Incrementeel zoeken, ook bekend als &#8220;vinden terwijl u typt&#8221;
+ Ondersteuning voor zoeken & vervangen in meerdere regels
+ Ondersteuning van reguliere expressie
+ Ondersteuning van zoeken & vervangen in meerdere geopende bestanden of bestanden op de schijf

## Reservekopie en herstellen

![Schermafdruk met herstel na een crash van Kate](/images/kate-crash.png)

+ Reservekopie bij opslaan
+ Bestanden omwisselen om gegevens te herstellen na crash van systeem
+ Systeem voor ongedaan maken / opnieuw doen
