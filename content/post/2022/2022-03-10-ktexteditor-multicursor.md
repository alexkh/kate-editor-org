---
title: Long live multicursors
author: Waqar Ahmed
date: 2022-03-10T22:43:00+05:00
url: /post/2022/2022-03-10-ktexteditor-multicursor/
---

Ever since I started contributing to Kate there has been one feature request that was being requested in almost every blog post or discussion about kate: multiple cursor support. I am happy to say that after 3 or 4 days of really pushing myself to implement this and address all the issues we just merged it (KDE Frameworks 5.92)! It will be available not only for Kate users, but for KDevelop, KWrite and any application that uses KTextEditor as its editor component.

## Short tutorial on using multiple cursors

- To create them via mouse, use `Alt + Click`
- To create via keyboard, `Ctrl + Alt + Down / Up`
- To create cursors out of a selection, first select some text and then press `Alt + Shift + I`

## What features are supported?

Right now we probably miss a lot of things and we need the community's help and ideas to make this feature awesome! But here's what's supported

- Basic text insertion and removal
- Auto completion
- Text transforms (upper/lower case, commenting, removing lines etc.)
- Movements (word, line, single character movement)
- multi select copy/cut

Did I miss some feature that you were really hoping for? Let us know, that's the only way it will get added. We really need the community's help in test and stabilizing this feature as
we want things to be stable before the release which is in about 25 days. So, if you feel like giving a hand feel free to come by the repo and say hi!
