---
title: Akademy 2016
author: Christoph Cullmann

date: 2016-09-01T10:55:26+00:00
url: /2016/09/01/akademy-2016/
categories:
  - Common
  - Events
  - KDE
tags:
  - planet

---
Arrived at Berlin for [Akademy/QtCon 2016][1], too ;=)

Already met some other KDE & Kate people, lets see how the week goes.

[<img class="aligncenter wp-image-3950 size-full" src="/wp-content/uploads/2016/09/Going-To-Akademy-2016.png" alt="Going-To-Akademy-2016" width="969" height="237" srcset="/wp-content/uploads/2016/09/Going-To-Akademy-2016.png 969w, /wp-content/uploads/2016/09/Going-To-Akademy-2016-300x73.png 300w, /wp-content/uploads/2016/09/Going-To-Akademy-2016-768x188.png 768w" sizes="(max-width: 969px) 100vw, 969px" />][1]

 [1]: https://akademy.kde.org/2016