---
title: Kate/KWrite Application Bundle Update
author: Christoph Cullmann

date: 2016-06-13T10:11:31+00:00
url: /2016/06/13/katekwrite-application-bundle-update/
categories:
  - Common
  - KDE
tags:
  - planet

---
I just created new bundles for Kate & KWrite.

By accident they are a bit large, as stuff as web engine is moved to the bundle, but they should work ;=)

How to create them?

mac.txt in kate.git shows how that works.

But you need a macqtdeploy with the patch for [QTBUG-48836][1].

<img class="aligncenter wp-image-3775 size-large" src="/wp-content/uploads/2016/06/Bildschirmfoto-2016-06-13-um-12.08.18-1024x721.png" alt="Bildschirmfoto 2016-06-13 um 12.08.18" width="474" height="334" srcset="/wp-content/uploads/2016/06/Bildschirmfoto-2016-06-13-um-12.08.18-1024x721.png 1024w, /wp-content/uploads/2016/06/Bildschirmfoto-2016-06-13-um-12.08.18-300x211.png 300w, /wp-content/uploads/2016/06/Bildschirmfoto-2016-06-13-um-12.08.18-768x541.png 768w" sizes="(max-width: 474px) 100vw, 474px" /> 

Download links:

  * [Kate 16.07.70][2]
  * [KWrite 16.07.70][3]

Thanks to the Randa meetup I got time for this, if you want to sponsor, click the image below ;)

[<img class="alignnone" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" />][4]

 [1]: https://codereview.qt-project.org/#/c/162160/
 [2]: ftp://babylon2k.de/cullmann/kate-16_07_70.dmg
 [3]: ftp://babylon2k.de/cullmann/kwrite-16_07_70.dmg
 [4]: https://www.kde.org/fundraisers/randameetings2016/