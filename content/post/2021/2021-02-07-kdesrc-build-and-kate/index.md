---
title: Setup your KDE development environment - kdesrc-build & Kate
author: Christoph Cullmann
date: 2021-02-07T16:35:00+02:00
url: /post/2021/2021-02-07-kdesrc-build-and-kate/
---

## KDE's developer experience evolution

Kate (and KDE) is always in need of more contributors.

Over the years we tried to make the development experience more pleasant and move to tools that are more widely adopted by developers around the world.

We traveled from ancient CVS repositories over to Subversion and since years are up and running on Git.

We moved our code hosting to a [more beginner friendly GitLab instance](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/) in the last year, too.

And I really think this does seem to show effects, at least for Kate & related projects we got a nice influx of contributions over GitLab.

Even just this years for Kate & Co. we merged a nice number of patches:

- 45 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 22 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 61 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 6 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

For more details, visit the regularly regenerated [merge requests page](/merge-requests/).

Beside the purely "what VCS do we use" and "which nifty web site for that" do we run, naturally the software components them-self got streamlined.

In the "good old" past, you had a nice thick kdelibs & kdebase repository that you did build to e.g. get Kate and it's parts.

Today, this has been split up into more manageable chunks.

You want to use the good old Kate syntax highlighting in your project?
No problem, this is [now an own separate framework](/2016/11/15/ksyntaxhighlighting-a-new-syntax-highlighting-framework/) just relying on Qt.

You want to build Kate?
No need to build e.g. KHTML!

Still, the initial setup of a Kate/KDE development environment seems still to be an issue for a lot of beginners.

## Kate's current status quo

On our [build it](/build-it/) page we describe how you can build Kate from kate.git manually on e.g. recent Linux distributions.
Beside that we delegate the building on e.g. [Windows](https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Windows) or [macOS](https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Mac) to Craft.

I still think the most common developer setup use case is at the moment on Linux (or fellow BSDs).
Unfortunately we didn't really keep our build description up-to-date for that, e.g. the package list we mention there is ancient.
And we didn't really outline how you can properly build the KDE Frameworks we require if you want to work on them, too.

## kdesrc-build for the win!

What could be improved there?

Beside [Craft](https://community.kde.org/Craft), that is very nifty to use to get stuff build, a tool that actually is in heavy use by KDE developers was somehow ignored in our beginners guide for Kate building: [kdesrc-build](https://kdesrc-build.kde.org/).

I use kdesrc-build since years for my local compiles and it is prominently promoted for e.g. [KDE Applications/Frameworks development](https://community.kde.org/Get_Involved/development#Building_software_with_kdesrc-build).

Kate learned some new tricks in the last years, too, that got not really promoted in our build setup guide: we have [proper LSP support](/post/2020/2020-01-01-kate-lsp-client-status/)!

But if you miss to e.g. do some tricks like creating the compilation database via CMake and linking it to the right place, this won't work out of the box so far.

kdesrc-build now has [some features](https://invent.kde.org/sdk/kdesrc-build/-/merge_requests/82) to automate this for you!

## New Kate development setup best practice

Now, then, let's start to illustrate how the proposed new Linux/BSD/... starter setup shall look.

### Choose your KDE development directory

Choose yourself some path where all KDE development things should end up.
Beside user local configuration files, nothing outside of this directory will be polluted.

For the remaining parts of this description we use

{{< highlight bash >}}
~/projects/kde
{{< / highlight >}}

Feel free to customize this.

### Install kdesrc-build

Installing is more or less just cloning the current version

{{< highlight bash >}}
mkdir -p ~/projects/kde/src
cd ~/projects/kde/src
git clone https://invent.kde.org/sdk/kdesrc-build.git
{{< / highlight >}}

For later ease of use, best symlink the kdesrc-build script to some folder inside your path, e.g. if you have some user local bin:

{{< highlight bash >}}
ln -s ~/projects/kde/src/kdesrc-build/kdesrc-build ~/bin
{{< / highlight >}}

### Configuring kdesrc-build

kdesrc-build has some setup pass that can configure stuff for you, see [this introduction](https://community.kde.org/Get_Involved/development#Set_up_kdesrc-build).

Here we just show a **.kdesrc-buildrc** that is good enough for Kate development needs.

You can just copy and paste the below variant into your home directory as **.kdesrc-buildrc** and adapt the paths to your needs.
If you stick with the **~/projects/kde** path we did choose above, this should be usable 1:1.

{{< highlight bash >}}
global
    # use the latest KF5 and Qt5-based software.
    branch-group kf5-qt5

    # we want .kateproject files with ninja
    cmake-options -G "Kate - Ninja"

    # clangd tooling
    compile-commands-export yes
    compile-commands-linking yes

    # Install directory for KDE software
    kdedir ~/projects/kde/usr

    # Directory for downloaded source code
    source-dir ~/projects/kde/src

    # Directory to build KDE into before installing
    # relative to source-dir by default
    build-dir ~/projects/kde/build
end global

include ~/projects/kde/src/kdesrc-build/kf5-frameworks-build-include
include ~/projects/kde/src/kdesrc-build/kf5-applications-build-include
include ~/projects/kde/src/kdesrc-build/kf5-workspace-build-include
include ~/projects/kde/src/kdesrc-build/kf5-extragear-build-include
{{< / highlight >}}

The important lines to have a good experience developing Kate (or other KDE stuff) with Kate are described below in detail.

#### Create .kateproject files

{{< highlight bash >}}
    # we want .kateproject files with ninja
    cmake-options -G "Kate - Ninja"
{{< / highlight >}}

This ensures you get not only Ninja build files (nifty as this will e.g. automatically use multiple cores for compiles) but in addition .kateproject files inside the build directories for later use, too.
With this files, the project plugin of Kate will know what to do, e.g. where the source directory for the compile is and which build commands it should use for the build plugin.

#### Ensure LSP integration works

{{< highlight bash >}}
    # clangd tooling
    compile-commands-export yes
    compile-commands-linking yes
{{< / highlight >}}

This ensures CMake will generate the **compile_commands.json** files that are required for clangd based LSP integration.
Beside just generating them inside the build directory the linking option will symlink them back to your source directories.
This allows zero-configuration usage of the LSP plugin inside Kate (and other editors).

### Install needed dependencies

**kdesrc-build** provides some initial setup mode to install the needed packages for several common distributions to start developing.
To do that, just trigger:

{{< highlight bash >}}
kdesrc-build --initial-setup
{{< / highlight >}}

We already created a **.kdesrc-buildrc**, that will be left untouched.
For the further usage you don't need to modify your shell settings either.

### Build Kate & dependencies

To trigger a compile of Kate and all needed KDE dependencies now just type:

{{< highlight bash >}}
kdesrc-build --include-dependencies kate
{{< / highlight >}}

For me, this did now compile 76 modules ;)
That takes a few minutes on a recent machine.
Time to grab some coffee or think about what you actually want to implement.

If you only want to build Kate without the dependencies because you are sure you have recent enough stuff on your system, you can try:

{{< highlight bash >}}
kdesrc-build --no-include-dependencies kate
{{< / highlight >}}

But given that on modern machines, the compile times are low, it is more convenient to just build all stuff, that ensures you have e.g. the latest and best KSyntaxHighlighting and KTextEditor frameworks around, too!

### How to use the build stuff?

To launch you local Kate version, you need to setup the environment first properly to ensure the right plugins and stuff is loaded.
Fortunately this is very simple:

{{< highlight bash >}}
source ~/projects/kde/build/kde/applications/kate/prefix.sh
kate
{{< / highlight >}}

A nifty way to wrap this is to e.g. create yourself a small wrapper script to start your master branch version of Kate inside your local bin directory:

{{< highlight bash >}}
#!/bin/bash
source ~/projects/kde/build/kde/applications/kate/prefix.sh
exec kate "$@"
{{< / highlight >}}

### Keep your stuff up-to-date

To keep your local version up-to-date, you can just use the above commands again.
They will take care of pulling new changes from the KDE repositories and building/installing them into your local prefix.

### Develop!

Now, the remaining question is: How to develop best?

Naturally, if you want to hack on Kate, it might make sense to use Kate for that.

Given the above preparations, that is easy to do, just start your new master version of Kate and pass it the build directory:

{{< highlight bash >}}
kate ~/projects/kde/build/kde/applications/kate
{{< / highlight >}}

Alternatively, you can navigate there in your terminal and startup Kate from there, it will auto-open it:

{{< highlight bash >}}
cd ~/projects/kde/build/kde/applications/kate
kate
{{< / highlight >}}

To have the best experience with this, ensure you have at least project & LSP plugin enabled.
If you like to have some GUI build integration, activate the build plugin, too.

You will end up with a new Kate windows like shown below.

<p align="center">
    <a href="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-startup.png" target="_blank"><img width=700 src="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-startup.png"/></a>
</p>

In the lower "Current Project" tool view you have per default two terminals.
The first terminal is inside your build directory, here you can e.g. run your **ninja** and **ninja install** commands and such.
The second terminal is inside your source directory, perfect for e.g. git command line calls.

Given the above setup, the LSP plugin (if you have **clangd** installed) should work out of the box.

<p align="center">
    <a href="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-lsp.png" target="_blank"><img width=700 src="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-lsp.png"/></a>
</p>

Other nifty stuff like project wide quick open, search & replace and correct build targets should be setup, too.

<p align="center">
    <a href="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-search.png" target="_blank"><img width=700 src="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-search.png"/></a>
</p>

<p align="center">
    <a href="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-build.png" target="_blank"><img width=700 src="/post/2021/2021-02-07-kdesrc-build-and-kate/images/kate-project-build.png"/></a>
</p>

## Future work

We will update our [build it](/build-it/) guide based on the above setup.
I think the **kdesrc-build** variant is the most easy way to get started for beginners on Unices.
Naturally, if you don't like to use that, you can do all stuff manually, too, that won't change.

Beside this, there is ongoing work in the project plugin to allow you to handle multiple projects at once easier, e.g. to have one meta project linking the individual ones.
But that support is still not fully ready.

## Now: Start Hacking!

Show up and [contribute](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/).

We have [a lot of ideas](https://invent.kde.org/utilities/kate/-/issues/20) and need help to get them done!

Or even better: show up with ideas on your own and discuss/implement them with us.

## Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/leopg9/setup_your_kde_development_environment/).
