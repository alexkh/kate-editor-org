---
title: KDevelop 4.2 Beta 1 released
author: Milian Wolff

date: 2010-12-17T20:49:29+00:00
excerpt: |
  Hey all,
  
  I&#8217;m happy to announce the immediate availability of KDevelop 4.2 Beta 1. This is a testing version and any kind of feedback would be welcome and appreciated. Please use our bugtracker. You can download the sources or wait for your distr...
url: /2010/12/17/kdevelop-4-2-beta-1-released/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
syndication_source_id:
  - http://milianw.de/tag/kate/feed
"rss:comments":
  - 'http://milianw.de/blog/kdevelop-42-beta-1-released#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/161
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/kdevelop-42-beta-1-released
syndication_item_hash:
  - d05c4013f3bba71ab7eda7c7dac5464c
  - 129d80321d873ec1e79bf7dac36a8bed
categories:
  - KDE

---
Hey all,

I&#8217;m happy to announce the immediate availability of KDevelop 4.2 Beta 1. This is a testing version and any kind of feedback would be welcome and appreciated. Please use our [bugtracker][1]. You can [download the sources][2] or wait for your distribution to create packages.

**Note:** Anyone who runs KDE 4.6 or later needs this version (or any later). This is because the <span class="geshifilter"><code class="text geshifilter-text">SmartRange</code></span> interface in Kate got nuked and replaced with <span class="geshifilter"><code class="text geshifilter-text">MovingRange</code></span> interface. Many thanks to David Nolden for more or less single handedly porting KDevelop to this new architecture.

The good news doesn&#8217;t end here though, KDevelop 4.2 ships with lots of notable changes and new features:

  * MovingRange\` support (Thanks to David Nolden)
  * [Grep Plugin rewrite with replace functionality][3] (Thanks to Silvere Lestang, Julien Desgats, Benjamin Port)
  * [QtHelp plugin now supports arbitrary <span class="geshifilter"><code class="text geshifilter-text">.qch</code></span> files][4] (Thanks to Benjamin Port)
  * [ManPage plugin][5] (Thanks to Benjamin Port, Yannick Motta)
  * new look for code assistants (Thanks to Olivier JG)
  * filter for the project model view (Thanks to Eugene Agafonov)
  * improved &#8220;Problems&#8221; toolview, along with &#8220;TODO/FIXME&#8221; browsing (Thanks to Dmitry Risenberg)
  * better standards compliance in the C++ macro implementation (Thanks to Dmitry Risenberg)
  * argument dependent lookup for the C++ plugin (Thanks to Ciprian Ciubotariu)
  * open with external application by default (esp. useful for Qt Designer <span class="geshifilter"><code class="text geshifilter-text">.ui</code></span> files)
  * PHP: rainbow colors for all vars (and no nested functions or similar), esp. useful for scripts without Classes/Functions
  * code cleanup for working sets
  * better handling of locked sessions (locked session gets activated or the user gets asked whether he wants to remove the lock file)
  * improved Snippets editing usability

Along with these big changes, lots of small improvements have been made and we will continue to polish and harden out this branch in order to give you a stable, fast KDevelop 4.2 soon. Considering the impact and amount of changes, testing would be _very_ welcome in order to prevent regressions and similar in the final 4.2 version.

Bye and thanks again to all contributors who made this possible. I&#8217;m especially thrilled to welcome so many new people, well done!

 [1]: http://bugs.kde.org
 [2]: http://download.kde.org/download.php?url=unstable/kdevelop/4.1.80/src/
 [3]: http://blog.ben2367.fr/2010/12/02/find-and-replace/
 [4]: http://blog.ben2367.fr/2010/11/11/kdevelop-when-development-documentation-come-to-you/
 [5]: http://blog.ben2367.fr/2010/12/01/man-page-documentation-in-kdevelop/