---
title: kate-editor.org moved
author: Christoph Cullmann

date: 2010-08-14T14:10:56+00:00
url: /2010/08/14/kate-editor-org-moved/
categories:
  - Common
tags:
  - planet

---
The website is now on a new server, which is a lot faster than the old one and software/hardware is fresh again.  
Hope the problems we had in the past few weeks are over now.  
No other real downtimes are planned, perhaps some reboots but nothing more than some minutes ;)  
Hope I can now finally setup my planned automated unit-tests and build ;)  
The DNS transfer seems to be faster than thought, still some people might arrive at the old server, where now only a stupid &#8220;Maintainance&#8221; page is up. The IP of the new server is 85.214.230.250.