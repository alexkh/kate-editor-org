---
title: Kate - User Feedback - August 2020
author: Christoph Cullmann
date: 2020-08-02T13:28:00+02:00
url: /post/2020/2020-08-02-kate-user-feedback/
---

Starting with the 20.04 release, Kate allows the user to turn on some telemetry submission.
This is a purely *opt-in* approach.
We do not submit any data if you not *actively enable* this feature!
For details about the introduction of this feature, see [this post](/post/2020/2020-02-09-kate-telemetry/).

After some months of having this out in the wild, I think it is time to show up with the first data we got.

### Submission numbers

First, how many submission do we get?

Actually, not that many, even in July we got just ~800 data submission.
As we hopefully have a lot more active users, it seems that not a large percentage of the users actually turned on user feedback submission.
For the installations via the [Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW) we know we have ~20.000 active users in the lasts months per month (with some telemetry enabled), I would assume we have more on Unices.

<p align="center">
    <a href="/post/2020/2020-08-02-kate-user-feedback/images/kate-user-feedback-samples.png" target="_blank"><img width=700 src="/post/2020/2020-08-02-kate-user-feedback/images/kate-user-feedback-samples.png"></a>
</p>

What can already be learned from the data we got?

### Kate versions in use

Most people use the the stable 20.04.x series and not some developer snapshot at the moment.
And one sees that e.g. the 20.04.0 really got replaced by most users with one of the patch releases.
How fast people really update from release to release needs to be seen, given the code is in since 20.04 and 20.08 is not yet out, that can't be seen at the moment.

<p align="center">
    <a href="/post/2020/2020-08-02-kate-user-feedback/images/kate-user-feedback-application-versions.png" target="_blank"><img width=700 src="/post/2020/2020-08-02-kate-user-feedback/images/kate-user-feedback-application-versions.png"></a>
</p>

### Qt versions in use

Next interesting data: which Qt versions are in use?
Here, most people are already on Qt 5.15.

<p align="center">
    <a href="/post/2020/2020-08-02-kate-user-feedback/images/kate-user-feedback-qt-versions.png" target="_blank"><img width=700 src="/post/2020/2020-08-02-kate-user-feedback/images/kate-user-feedback-qt-versions.png"></a>
</p>

That is on one side nice, as the latest Qt version is the version I use most often for testing.
On the other side, Qt 5.15.0 unfortunately has some breakage in the QJSEngine we use (actually, Qt 5.14, too), that means, if you see random crashes with backtraces into the QJSEngine parts, hope for some 5.15.x update (or backports done by your distribution).
We can't really work around that (beside if we would use a completely different engine, that would be a major effort).

Let's see how the feedback submissions develop in the next months.
Thanks already to all people already providing feedback!

### Discussion

If you want to chime in on this, feel free to join the discussion at the [KDE reddit](https://www.reddit.com/r/kde/comments/i2an0o/kate_user_feedback_august_2020/).

If you want to contribute, just join us at [invent.kde.org](https://invent.kde.org/utilities/kate) or on [kwrite-devel@kde.org](mailto:kwrite-devel@kde.org).
