---
title: Windows Store Status
author: Christoph Cullmann
date: 2020-02-08T17:43:00+02:00
url: /post/2020/2020-02-08-windows-store-status/
---

The Elisa music player is now in the [Windows Store](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL), too!

It is the sixth application published there with the [KDE e.V.](https://ev.kde.org/) account.

And here are our current number of acquisitions (roughly equal to the number of installations, not mere downloads) for our applications:

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) - 23,524 acquisitions

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) - 16,154 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) - 1,412 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) - 1,400 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) - 1,343 acquisitions

* [Elisa - Modern Music Player](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL) - 40 acquisitions

If you want to help to bring more stuff KDE develops on Windows, we have some meta [Phabricator task](https://phabricator.kde.org/T9575) were you can show up and tell for which parts you want to do work on.

A guide how to submit stuff later can be found [on our blog](/post/2019/2019-11-03-windows-store-submission-guide/).

I hope the number of applications will grow further in the near future ;=)

Thanks to all the people that help out with submissions & updates & fixes!
