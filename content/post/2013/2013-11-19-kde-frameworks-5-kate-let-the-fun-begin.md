---
title: 'KDE Frameworks 5 & Kate, let the fun begin :)'
author: Christoph Cullmann

date: 2013-11-19T21:42:03+00:00
url: /2013/11/19/kde-frameworks-5-kate-let-the-fun-begin/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
tags:
  - planet

---
After thinking some days about how to tackle the 4.x => 5.x transition in Kate (KTextEditor/Part/Application) and the nice &#8220;what we should do&#8221; blog by Dominik, I think the time for fun is there.

Therefore I started to port our stuff to KF5 in the <a title="First commit to frameworks in kate.git" href="http://quickgit.kde.org/?p=kate.git&a=commit&h=12beef19865de5decb4f3ef8099fea7f1407066c" target="_blank">&#8220;frameworks&#8221;</a> branch.

The basic idea would be: get it compiling and running.

Then we can decide if the frameworks branch is a mere &#8220;hack to see if it works&#8221; experiment which can be later used to port master without a lot of work or if we say &#8220;ok, works kind of well&#8221; and we just switch development over for new features from master to frameworks and with that from 4.x to 5.x.

The KTextEditor interfaces compile now, KatePart should be the next thing to tackle.  Then KWrite and Kate.

Happy hacking ;)

(To get a working KF5 development environment, just follow <a title="KF5 Build Howto" href="http://community.kde.org/Frameworks/Building" target="_blank">this howto</a>.)