---
title: 'Kate: More on Indentation Scripting'
author: Dominik Haumann

date: 2007-07-20T17:18:00+00:00
url: /2007/07/20/kate-more-on-indentation-scripting/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2007/07/kate-more-on-indentation-scripting.html
categories:
  - Users

---
<a href="/2007/07/18/kate-scripting-indentation/" target="_self">My last blog</a> was about the theory of how indentation works by using javascripts. Now we will look at a concrete example: a LISP-style indenter. (LISP indentation is easy, that&#8217;s why it&#8217;s a good example).  
The rules:

  * comments starting with ;;; always have indentation 0
  * comments starting with ;; should be aligned with the next line
  * comments starting with ; should only appear behind code, so they are simply ignored
  * every &#8216;(&#8216; indents and every &#8216;)&#8217; unindents

lisp.js looks like this:

> <span style="font-family: courier new;">/** kate-script<br /> * name: LISP<br /> * license: LGPL<br /> * author: foo bar<br /> * version: 1<br /> * kate-version: 3.0<br /> * type: indentation<br /> */<br /> // indent should be called when ; is pressed<br /> triggerCharacters = &#8220;;&#8221;;<br /> function indent(line, indentWidth, ch)<br /> {<br /> // special rules: ;;; -> indent 0<br /> //                ;;  -> align with next line, if possible<br /> //                ;   -> usually on the same line as code -> ignore<br /> textLine = document.line(line);<br /> if (textLine.search(/^\s*;;;/) != -1) {<br /> return 0;<br /> } else if (textLine.search(/^\s*;;/) != -1) {<br /> // try to align with the next line<br /> nextLine = document.nextNonEmptyLine(line + 1);<br /> if (nextLine != -1) {<br /> return document.firstVirtualColumn(nextLine);<br /> }<br /> }</p> 
> 
> <p>
>   cursor = document.anchor(line, 0, &#8216;(&#8216;);<br /> if (cursor) {<br /> return document.toVirtualColumn(cursor.line, cursor.column) + indentWidth;<br /> } else {<br /> return 0;<br /> }<br /> }
> </p>
> 
> <p>
>   </span>
> </p></blockquote> 
> 
> <p>
>   The result:
> </p>
> 
> <blockquote>
>   <p>
>     <span style="font-family: courier new;">;;; fib : number -> number<br /> (define (fib n)<br /> (if (< n 2)<br /> 1<br /> (+ (fib (- n 1)) (fib (- n 2)))))</span>
>   </p>
> </blockquote>
> 
> <p>
>   As this indenter is scripted, everybody can adapt the style to the own needs.
> </p>