---
title: Kate in 4.12
author: Dominik Haumann

date: 2014-01-09T10:20:30+00:00
url: /2014/01/09/kate-in-4-12/
categories:
  - Developers
  - Users
tags:
  - planet
  - vi input mode

---
Since the KDE SC 4.12 release a month ago, it&#8217;s about time to look at the changes of Kate in 4.12:

  * <span style="line-height: 1.5;">Vi input mode: </span><a style="line-height: 1.5;" title="Macro support in Kate's vi input mode" href="https://bugs.kde.org/show_bug.cgi?id=288351" target="_blank">complete macro support</a>
  * Heavily improved [vi input mode][1]
  * [Multi-column text editing][2] (thanks to Andrey Matveyakin)
  * MiniMap: align to the top (instead of vertically centered)
  * Projects plugin: modified files now show a modified icon
  * Improved [C++/Boost Style indenter][3]
  * in total, <a title="Kate bug fixes for KDE SC 4.12" href="https://bugs.kde.org/buglist.cgi?resolution=FIXED&resolution=WORKSFORME&resolution=UPSTREAM&resolution=DOWNSTREAM&query_format=advanced&bug_status=RESOLVED&bug_status=CLOSED&longdesc=4.12&longdesc_type=allwordssubstr&product=kate&list_id=900016" target="_blank">21 issues</a> (including wishes and bugs) were fixed

### What comes next?

Kate will get more polishing in the next 4.x releases, for instance, in KDE SC 4.13 Kate optionally supports [animated bracket matching][4].

However, as already mentioned in [Kate in 4.11][5], <span style="line-height: 1.5;">the main efforts are put into making Kate on Qt5 and Frameworks 5 rock solid. Already now Kate, KWrite and Kate Part are fully ported, i.e. all are free of KDE4support libraries. A preview (already one month old!) can be <a title="Kate on 5" href="/2013/12/08/kate-on-kde-frameworks-5/">found here</a>.</span>

Besides that, Kate and KDevelop again join forces and there is a developer meeting from 18th to 25th of January 2014. So expect quite some blogs about Kate on 5 then!

 [1]: /2013/09/06/kate-vim-progress/ "Improved Vi input mode"
 [2]: /2013/09/09/multi-line-text-editing-in-kate/ "Multi-column text editing in Kate"
 [3]: /2013/12/01/kate-cboost-style-indenter/ "C++/Boost Style indenter"
 [4]: /2013/11/06/animated-bracket-matching-in-kate-part/ "Animated bracket matching in Kate"
 [5]: /2013/09/09/kate-in-4-11/ "Kate in 4.11"