---
title: Qt open governance will go live!
author: Christoph Cullmann

date: 2011-09-12T18:15:59+00:00
url: /2011/09/12/qt-open-governance-will-go-live/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
Finally some sustainable announcements about the future of Qt and its open governance project:

<http://labs.qt.nokia.com/2011/09/12/qt-project/>

<http://blog.qt.nokia.com/2011/09/12/qt-project/>

I hope this will be well received and we will see yet another increase in contributions to our Qt/KDE ecosystem ;)

Thanks for all the effort to all people who made this possible!