---
title: Kate needs a Face, Artists wanted
author: Christoph Cullmann

date: 2011-08-15T15:29:16+00:00
url: /2011/08/15/kate-needs-a-face-artists-wanted/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---

**2021 Update**: Kate's current mascot is [Kate the Cyber Woodpecker](/mascot/).

Kate&#8217;s icon is quiet nice, but for our website and other stuff, it would be really cool to have some eye catcher and something to know: yeah, that is Kate :)

As Kate is obviously a woman, at least in most languages, it would be quiet nice if some artist could come up with a painted or rendered one to put on our page.

Perhaps something [Sintel][1] like, at least she played with dragons (hope poor Konqui got not killed, too) and I guess a mascot like here won&#8217;t scare of either men or women.

[<img class="aligncenter size-full wp-image-1283" title="Sintel, Copyright Blender Foundation, CC Attribution 3.0" src="/wp-content/uploads/2011/08/sintel-paintover-wip05.jpeg" alt="" width="600" height="841" srcset="/wp-content/uploads/2011/08/sintel-paintover-wip05.jpeg 600w, /wp-content/uploads/2011/08/sintel-paintover-wip05-214x300.jpg 214w" sizes="(max-width: 600px) 100vw, 600px" />][2]

And no, we don&#8217;t want here any men&#8217;s dream images or sexist stuff like that, just something that looks cute. If you want to help with this, just drop a mail to either [me][3] or the [kwrite-devel list][4]. Be aware, it must be original work and it should be licensed under something like the CC license.

If nothing arrives for us, perhaps I really need to bug the Blender foundation to use their stuff for the website. Better eye catcher than my normally used insect of the day ;)

 [1]: http://www.sintel.org/
 [2]: /wp-content/uploads/2011/08/sintel-paintover-wip05.jpeg
 [3]: mailto:cullmann@kde.org
 [4]: mailto:kwrite-devel@kde.org
