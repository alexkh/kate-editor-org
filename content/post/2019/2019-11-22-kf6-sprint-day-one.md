---
title: KF6 Sprint - Day One
author: Christoph Cullmann
date: 2019-11-22T16:00:00+02:00
url: /post/2019/2019-11-22-kf6-sprint-day-one/
---

Today we started our [KF6 sprint](https://www.volkerkrause.eu/2019/11/01/kf6-sprint-annoucment.html) at the [MBition](https://mbition.io/en/home/index.html) office in Berlin.

Beside the people [attending in person](https://community.kde.org/Sprints/KF6), we have David Faure joining us via web conference.

Thanks already to the people at MBition that spend time on making it possible to host the sprint there.

First stuff to be discussed were some high level things, like does the monthly release scheme work out well.
Short answer: yes :)
The short period works well, allows people to fix issues directly in frameworks and still have that reasonable fast provided to the users.
And the overhead of release creation is low, thanks to automation.

Other fundamental stuff discussed: Is the tier model still a good fit for the dependency description, here the consensus was yes, too.
Perhaps with some tweaks for higher tier levels granularity.

On the other side, it seems clear, that some more cleanup and split-up of some frameworks is in order for the future.

Some problematic stuff like KWallet as framework + implementation of storage for secrets could be replaced with a proper framework that wraps around the proper platform specific service.

Later the discussion shifted more in the direction: Which frameworks might not make it to KF6 and what is the impact of that for the applications using it.

These were just some points we discussed, more details can be found in the [preliminary agenda with comments](https://share.kde.org/s/axSpBmfPxyySXd4).

For concrete work to be done, we have the tasks on [Phabricator](https://phabricator.kde.org/tag/kf6/).
I assume this task board will evolve considerably in the next days.
