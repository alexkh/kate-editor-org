---
title: Data Recovery in 4.10
author: Dominik Haumann

date: 2012-10-25T12:05:27+00:00
url: /2012/10/25/data-recovery-in-4-10/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
Recently I&#8217;ve blogged about the usage of [KMessageWidget in the data recovery process][1] in Kate Part. Finally, we decided to stick with KMessageWidget, since it is a standard kdelibs widget, used by a lot of KDE applications. Besides, it is visually appealing and attracts the user&#8217;s attention. In KDE SC 4.10, it will look like this:

<img class="aligncenter size-full wp-image-2044" title="Data Recovery" src="/wp-content/uploads/2012/10/recovery.png" alt="" width="495" height="333" srcset="/wp-content/uploads/2012/10/recovery.png 495w, /wp-content/uploads/2012/10/recovery-300x201.png 300w" sizes="(max-width: 495px) 100vw, 495px" /> 

Now if you recover the data, it may happen that the swap file is broken, e.g. because it was accidently manipulated for whatever reason. Then you get notified like this:

<img class="aligncenter size-full wp-image-2045" title="Broken Data Recovery" src="/wp-content/uploads/2012/10/broken-recovery.png" alt="" width="500" height="333" srcset="/wp-content/uploads/2012/10/broken-recovery.png 500w, /wp-content/uploads/2012/10/broken-recovery-300x199.png 300w" sizes="(max-width: 500px) 100vw, 500px" /> 

Besides that, we are currently at our Kate/KDevelop meeting in Vienna. Lots of exciting stuff is happening, so expect more in the next days :-)

 [1]: /2012/07/08/rfc-data-recovery/ "RFC: Data Recovery"