---
title: On-the-fly spellchecking in Kate
author: Dominik Haumann

date: 2009-07-07T18:51:00+00:00
url: /2009/07/07/on-the-fly-spellchecking-in-kate/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2009/07/on-fly-spellchecking-in-kate.html
categories:
  - Developers
  - Users

---
Christoph just added an awesome and long awaited feature: on-the-fly spellchecking. &#8216;Kate&#8217;s xml highlighting files now have an additional attribute in the itemData section: spellChecking=&#8221;true/false&#8221;. C++ comments and strings can be spellchecked now :) Same for all other languages such as Latex. Really big thanks to Michel Ludwig for the patch, good work! Screenshot for latex highlighting:

<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://1.bp.blogspot.com/_JcjnuQSFzjw/SlOZuLvbdHI/AAAAAAAAADs/mX4dVlvZA50/s1600-h/snapshot1.png"><img style="cursor: pointer; width: 400px; height: 326px;" src="http://1.bp.blogspot.com/_JcjnuQSFzjw/SlOZuLvbdHI/AAAAAAAAADs/mX4dVlvZA50/s400/snapshot1.png" alt="" id="BLOGGER_PHOTO_ID_5355793400578405490" border="0" /></a>