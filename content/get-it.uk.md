---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Отримати Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux та UNIX-и

+ Встановіть [Kate](https://apps.kde.org/en/kate) або [KWrite](https://apps.kde.org/en/kwrite) зі [сховищ пакунків для вашого дистрибутива](https://kde.org/distributions/)

{{< appstream_badges >}}

+ [Пакунок Snap Kate у Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Випуск Kate (64-бітовий) у форматі пакунка AppImage](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Щоденна збірка Kate (64-бітова) у форматі пакунка AppImage](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Зберіть програму](/build-it/#linux) з початкових кодів.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate у Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate у Chocolatey](https://chocolatey.org/packages/kate)
+ [Пакунок для встановлення випуску Kate (64-бітова версія)](https://download.kde.org/stable/release-service/21.12.3/windows/kate-21.12.3-1589-windows-msvc2019_64-cl.exe) *
+ [Засіб встановлення щоденної версії Kate (64-бітовий)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Зберіть програму](/build-it/#windows) з початкових кодів.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Засіб встановлення випуску Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Засіб встановлення щоденної версії Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Зберіть програму](/build-it/#mac) з початкових кодів.

{{< /get-it >}}

{{< get-it-info >}}

**Про випуски:** <br> [Kate](https://apps.kde.org/en/kate) і [KWrite](https://apps.kde.org/en/kwrite) є частинами [збірки програм KDE](https://apps.kde.org), яку [типово випускають 3 рази на рік](https://community.kde.org/Schedules). [Рушій редагування тексту](https://api.kde.org/frameworks/ktexteditor/html/) та [підсвічування синтаксису](https://api.kde.org/frameworks/syntax-highlighting/html/) є частинами бібліотек [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), які [оновлюють щомісяця](https://community.kde.org/Schedules/Frameworks). Про нові випуски оголошують [тут](https://kde.org/announcements/).

\* У цих пакунках **випуску** містяться дані найсвіжішої версії Kate і KDE Frameworks.

\*\* **Щоденні** пакунки автоматично збираються з початкового коду щодня, тому вони можуть бути нестабільними у роботі і містити вади або можливості, які реалізовано не повністю. Рекомендуємо користуватися ними лише з метою тестування.

{{< /get-it-info >}}
